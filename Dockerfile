FROM eu.gcr.io/vseth-public/base:foxtrott

RUN echo "Package: nodejs" >> /etc/apt/preferences.d/preferences \
    && echo "Pin: origin deb.nodesource.com" >> /etc/apt/preferences.d/preferences \
    && echo "Pin-Priority: 1001" >> /etc/apt/preferences.d/preferences
RUN apt install -y ca-certificates curl gnupg
RUN mkdir -p /etc/apt/keyrings
RUN curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg

RUN echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_20.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list
RUN apt update
RUN apt install -y nodejs python3 python3-pip

RUN mkdir -p /newsletter
WORKDIR /newsletter
COPY package.json /newsletter

RUN npm install --force
COPY . /newsletter
ENV DATABASE_URL="mysql://user:password@localhost:5432/dummy"
RUN npx prisma generate
RUN npm run build

WORKDIR /newsletter/renderer
RUN python3 -m pip install --break-system-packages --no-cache-dir --upgrade pip && python3 -m pip install --break-system-packages --no-cache-dir  -r requirements.txt

COPY cinit.yml /etc/cinit.d/newsletter.yml

EXPOSE 3000
