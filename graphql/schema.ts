export const typeDefs = `
  scalar Date

  type Newsletter {
    id: Int
    sendAt: Date
    isSent: Boolean
    entries: [Entry]
  }

  type Entry {
    id: Int
    newsletter: Newsletter
    submittedAt: String
    author: String
    authorMail: String
    organization: String
    organizationType: String
    entryType: String
    startAt: String
    includeStartTime: Boolean
    endAt: String
    includeEndTime: Boolean
    place: String
    titleDE: String
    titleEN: String
    descriptionDE: String
    descriptionEN: String
    signUp: String
    website: String
    contact: String
    isFeatured: Boolean
    isChecked: Boolean
    isDisabled: Boolean
  }

  type Query {
    getNewsletters: [Newsletter]!
    getNewsletter (id: Int): Newsletter
    getNewestNewsletter: Newsletter
    getUncheckedEntries (id: Int): Boolean
    getRenderedNewsletter (id: Int): String
    getNextNewsletterId: Int
    getNewestNewsletterId: Int
  }

  type Mutation {
    addNewsletter (sendAt: Date): Newsletter
    deleteNewsletter (id: Int): Newsletter
    deleteEntry (id: Int): Boolean
    editEntry (id: Int!, newsletterId: Int!, organization: String!, organizationType: String!, titleDE: String!, titleEN: String!, descriptionDE: String!, descriptionEN: String!, entryType: String!, place: String, startAt: Date, endAt: Date, includeStartTime: Boolean, includeEndTime: Boolean, signUp: String, website: String, contact: String, isFeatured: Boolean!, isChecked: Boolean!, isDisabled: Boolean!): Entry
    addEntry (newsletterId: Int!, author: String!, authorMail: String!, organization: String!, organizationType: String!, titleDE: String!, titleEN: String!, descriptionDE: String!, descriptionEN: String!, entryType: String!, place: String, startAt: Date, endAt: Date, includeStartTime: Boolean, includeEndTime: Boolean, signUp: String, website: String, contact: String, isDisabled: Boolean!): Entry
    sendReminder (html: String, receivers: [String]): Boolean
    sendNewsletter (id: Int, html: String, receivers: [String]): Boolean
    updateSentStatus (id: Int, status: Boolean): Boolean
  }
`;
