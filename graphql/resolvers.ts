import prisma from "../lib/prisma";
import { getServerSession } from "next-auth/next";
import authOptions from "@/lib/authOptions";
import getAuth from "../utilities/getAuth";
import dateScalar from "../graphql/dateScalar";
import nodemailer from "nodemailer";
import axios from "axios";

import getConfig from "@/config/getConfig";
import sort from "@/utilities/sort";

import { NewsletterType, EntryType } from "../interfaces/newsletter";

export const resolvers = {
  Date: dateScalar,
  Query: {
    // NOTE: Sorting might become slow with time --> keep an eye on this!
    getNewsletters: async () => {
      const newsletters = await prisma.newsletter.findMany({
        include: {
          entries: {
            where: {
              isDisabled: false,
            },
          },
        },
        orderBy: [{ sendAt: "asc" }],
      });
      newsletters.forEach((newsletter: any) => {
        sort(newsletter);
      });
      return newsletters;
    },
    getNewsletter: async (_: any, { id }: { id: number }) => {
      const newsletter = await prisma.newsletter.findUnique({
        where: { id },
        include: { entries: true },
      });
      sort(newsletter);
      return newsletter;
    },
    getNewestNewsletter: async () => {
      const newsletter = await prisma.newsletter.findFirst({
        where: { isSent: true },
        include: {
          entries: {
            where: {
              isDisabled: false,
            },
          },
        },
        orderBy: [{ sendAt: "desc" }],
      });
      newsletter && sort(newsletter);
      return newsletter;
    },
    getUncheckedEntries: async (_: any, { id }: { id: number }) => {
      const newsletter = await prisma.newsletter.findUnique({
        where: { id },
        include: { entries: true },
      });
      let found = false;
      if (!newsletter) return true;
      newsletter.entries.forEach((entry: any) => {
        if (!entry.isDisabled && !entry.isChecked) found = true;
      });
      return found;
    },
    getRenderedNewsletter: async (_: any, { id }: { id: number }) => {
      const newsletter = await prisma.newsletter.findUnique({
        where: { id },
        include: {
          entries: {
            where: {
              isDisabled: false,
            },
          },
        },
      });
      if (!newsletter) return "error";
      if (process.env.RENDERER == undefined) {
        throw new Error("the RENDERER environment variable is undefined");
        return false;
      }

      const html = await axios.get(process.env.RENDERER, {
        data: {
          newsletter: newsletter,
        },
      });

      return html.data;
    },
    getNextNewsletterId: async () => {
      const newsletter = await prisma.newsletter.findFirst({
        where: { isSent: false },
        orderBy: [{ sendAt: "asc" }],
      });
      return newsletter?.id || 0;
    },
    getNewestNewsletterId: async () => {
      const newsletter = await prisma.newsletter.findFirst({
        where: { isSent: true },
        orderBy: [{ sendAt: "desc" }],
      });
      return newsletter?.id || 0;
    },
  },
  Mutation: {
    addNewsletter: async (
      _: any,
      { sendAt }: { sendAt: Date },
      { session }: any,
    ) => {
      if (!getAuth(session, true)) return;

      const entity = await prisma.newsletter.create({
        data: {
          sendAt,
          isSent: false,
        },
      });
      return entity;
    },
    deleteNewsletter: async (
      _: any,
      { id }: { id: number },
      { session }: any,
    ) => {
      if (!getAuth(session, true)) return;

      const entity = await prisma.newsletter.delete({
        where: { id },
      });
      return entity;
    },
    deleteEntry: async (_: any, { id }: { id: number }, { session }: any) => {
      if (!getAuth(session, true)) return;

      await prisma.entry.delete({
        where: { id },
      });
      return true;
    },
    editEntry: async (_: any, data: any, { session }: any) => {
      if (!getAuth(session, true)) return;

      const entity = await prisma.entry.update({
        where: { id: data.id },
        data: {
          ...data,
        },
      });
      return entity;
    },
    addEntry: async (_: any, data: any, { session }: any) => {
      const entity = await prisma.entry.create({
        data: {
          ...data,
          isFeatured: false,
          isChecked: false,
        },
      });
      return entity;
    },
    sendReminder: async (
      _: any,
      { html, receivers }: { html: string; receivers: string[] },
      { session }: any,
    ) => {
      if (!getAuth(session, true)) return false;

      const config = await getConfig();

      const transporter = nodemailer.createTransport({
        host: process.env.MAILER_HOST,
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
          user: process.env.MAILER_USERNAME,
          pass: process.env.MAILER_PASSWORD,
        },
      });

      await receivers
        .filter((r: string) => r.replace(" ", "") != "")
        .map(async (r: string) => {
          await transporter.sendMail(
            {
              from: process.env.MAILER_NAME,
              to: r,
              replyTo: config.send.reply_to,
              subject: config.send.reminder_subject,
              html: html,
            },
            function (error, success) {
              if (error) {
                console.log(error);
              } else {
                console.log("Server is ready to take our messages");
              }
            },
          );
        });

      return true;
    },
    sendNewsletter: async (
      _: any,
      { id, receivers }: { id: number; receivers: string[] },
      { session }: any,
    ) => {
      if (!getAuth(session, true)) return;

      const config = await getConfig();

      const newsletter = await prisma.newsletter.findUnique({
        where: { id },
        include: {
          entries: {
            where: {
              isDisabled: false,
            },
          },
        },
      });
      sort(newsletter);

      if (process.env.RENDERER == undefined) {
        throw new Error("the RENDERER environment variable is undefined");
        return false;
      }

      if (!config.newsletter.template) {
        throw new Error("please define `newsletter.template` in the config");
        return false;
      }

      const html = await axios.get(process.env.RENDERER, {
        data: {
          newsletter: newsletter,
          template: config.newsletter.template,
        },
      });

      const transporter = nodemailer.createTransport({
        host: process.env.MAILER_HOST,
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
          user: process.env.MAILER_USERNAME,
          pass: process.env.MAILER_PASSWORD,
        },
      });

      receivers
        .filter((r: string) => r.replace(" ", "") != "")
        .map(async (r: string) => {
          await transporter.sendMail({
            from: process.env.MAILER_NAME,
            to: r,
            replyTo: config.send.reply_to,
            subject: config.send.subject,
            html: html.data,
          });
        });

      return true;
    },
    updateSentStatus: async (
      _: any,
      { id, status }: { id: number; status: boolean },
      { session }: any,
    ) => {
      if (!getAuth(session, true)) return;

      await prisma.newsletter.update({
        where: { id },
        data: {
          isSent: status,
        },
      });

      return true;
    },
  },
};
