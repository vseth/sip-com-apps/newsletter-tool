import { GraphQLScalarType, Kind } from "graphql";

const dateScalar = new GraphQLScalarType({
  name: "Date",
  description: "Date custom scalar type",
  serialize(value) {
    if (value instanceof Date) {
      return value; // Convert incoming integer to Date
    }
    throw new Error("GraphQL Date Scalar serializer expected a `Date`");
  },
  parseValue(value: any) {
    // doing the type checking manually
    const d = new Date(value);
    if (d instanceof Date) {
      return d;
    }
    throw Error("GraphQL Date Scalar parser expected a `Date` object");
  },
  parseLiteral(ast) {
    if (ast.kind === Kind.INT) {
      // Convert hard-coded AST string to integer and then to Date
      return new Date(parseInt(ast.value, 10));
    }
    // Invalid hard-coded value (not an integer)
    return null;
  },
});

export default dateScalar;
