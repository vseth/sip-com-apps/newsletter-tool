// @ts-ignore
import { PrismaClient } from "prisma/client";
import prisma from "../lib/prisma";

import { NextApiRequest, NextApiResponse } from "next";

export type Context = {
  prisma: any;
};

export async function createContext(
  req: NextApiRequest,
  res: NextApiResponse,
): Promise<Context> {
  return {
    prisma,
  };
}
