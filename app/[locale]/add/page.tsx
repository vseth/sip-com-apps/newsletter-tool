"use client";
export const dynamic = "force-dynamic";

import { useRouter } from "next/navigation";

import { useScopedI18n } from "@/locales/client";

import { Alert } from "@mantine/core";

import { gql, useQuery } from "@apollo/client";

const getNextNewsletterIdQuery = gql`
  {
    getNextNewsletterId
  }
`;

export default function AddForward() {
  const { data: id } = useQuery(getNextNewsletterIdQuery);
  const t = useScopedI18n("common");
  const router = useRouter();

  if (id && id.getNextNewsletterId !== 0) {
    router.push("/add/" + id.getNextNewsletterId);
  }

  return (
    <Alert variant="filled" title={t("addNextNewsletterTitle")}>
      {t("addNextNewsletterText")}
    </Alert>
  );
}
