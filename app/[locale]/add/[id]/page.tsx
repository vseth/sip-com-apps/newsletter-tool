"use client";
export const dynamic = "force-dynamic";

import { useState, useEffect } from "react";
import { useRouter } from "next/navigation";

import { useScopedI18n } from "@/locales/client";

import { notifications } from "@mantine/notifications";

import dayjs from "dayjs";

import {
  ActionIcon,
  Alert,
  Button,
  Container,
  Group,
  Select,
  Textarea,
  TextInput,
  Tooltip,
  useMantineTheme,
} from "@mantine/core";

import {
  IconClock,
  IconLocation,
  IconInfoCircle,
  IconCalendar,
  IconLink,
  IconMail,
  IconX,
} from "@tabler/icons-react";

import { DatePickerInput, TimeInput } from "@mantine/dates";

import { useForm } from "@mantine/form";

import EntryPreview from "@/components/entryPreview";
import type { addForm } from "@/interfaces/add";

import useConfig from "@/config/useConfig";

type ReducedNewsletterType = {
  value: string;
  label: string | null;
  sendAt: Date;
};

const addEntryMut = gql`
  mutation addEntry(
    $newsletterId: Int!
    $author: String!
    $authorMail: String!
    $organization: String!
    $organizationType: String!
    $titleDE: String!
    $titleEN: String!
    $descriptionDE: String!
    $descriptionEN: String!
    $entryType: String!
    $place: String
    $startAt: Date
    $includeStartTime: Boolean!
    $endAt: Date
    $includeEndTime: Boolean!
    $signUp: String
    $website: String
    $contact: String
    $isDisabled: Boolean!
  ) {
    addEntry(
      newsletterId: $newsletterId
      author: $author
      authorMail: $authorMail
      organization: $organization
      organizationType: $organizationType
      titleDE: $titleDE
      titleEN: $titleEN
      descriptionDE: $descriptionDE
      descriptionEN: $descriptionEN
      entryType: $entryType
      place: $place
      startAt: $startAt
      includeStartTime: $includeStartTime
      endAt: $endAt
      includeEndTime: $includeEndTime
      signUp: $signUp
      website: $website
      contact: $contact
      isDisabled: $isDisabled
    ) {
      id
    }
  }
`;

const getNewslettersQry = gql`
  query getNewsletters {
    getNewsletters {
      id
      sendAt
      isSent
    }
  }
`;

import { gql, useQuery, useMutation } from "@apollo/client";

import { dateFormatter } from "@/utilities/datetimeFormatter";
import { getTextColor } from "@/utilities/colors";

export default function Add({
  params: { locale, id },
}: {
  params: { locale: "de" | "en"; id: string };
}) {
  const [newsletters, setNewsletters] = useState<ReducedNewsletterType[]>([]);

  const t = useScopedI18n("form");
  const config = useConfig();
  const router = useRouter();
  const theme = useMantineTheme();

  const max_entry_length = config?.newsletter?.max_entry_length || 400;
  const correction_time = config?.newsletter?.correction_time || 48;

  const [addEntry] = useMutation(addEntryMut);
  useQuery(getNewslettersQry, {
    onCompleted: (data) => {
      formatNewsletters(data.getNewsletters);
    },
  });

  const initialValues: addForm = {
    newsletterId: null,
    author: "",
    authorMail: "",
    organization: "",
    organizationType: "",
    titleDE: "",
    titleEN: "",
    descriptionDE: "",
    descriptionEN: "",
    entryType: "",
    place: "",
    startAt: null,
    startTimeAt: "",
    endAt: null,
    endTimeAt: "",
    signUp: "",
    website: "",
    contact: "",
  };

  const form = useForm({
    initialValues: initialValues,

    transformValues: (values) => ({
      ...values,
      startAt: values.startAt
        ? new Date(
            values.startAt.getFullYear(),
            values.startAt.getMonth(),
            values.startAt.getDate(),
            values.startTimeAt
              ? Number(values.startTimeAt.split(":")[0])
              : new Date().getHours(),
            values.startTimeAt
              ? Number(values.startTimeAt.split(":")[1])
              : new Date().getHours(),
            values.startTimeAt ? 0 : new Date().getSeconds(),
          )
        : null,
      endAt: values.endAt
        ? new Date(
            values.endAt.getFullYear(),
            values.endAt.getMonth(),
            values.endAt.getDate(),
            values.endTimeAt
              ? Number(values.endTimeAt.split(":")[0])
              : new Date().getHours(),
            values.endTimeAt
              ? Number(values.endTimeAt.split(":")[1])
              : new Date().getMinutes(),
            values.endTimeAt ? 0 : new Date().getSeconds(),
          )
        : null,
    }),

    validate: {
      newsletterId: (value) => (value ? null : t("Enewsletter")),
      author: (value) => (value.length < 1 ? t("Ename") : null),
      authorMail: (value) =>
        /^[a-zA-Z0-9]+(?:\.[a-zA-Z0-9]+)*@[a-zA-Z0-9]+(?:\.[a-zA-Z0-9]+)*$/.test(
          value,
        )
          ? null
          : t("Email"),
      organization: (value) => (value.length < 1 ? t("Eorg") : null),
      organizationType: (value) => (value == "" ? t("EorgType") : null),
      titleDE: (value) => (value.length < 1 ? t("Etitle") : null),
      titleEN: (value) => (value.length < 1 ? t("Etitle") : null),
      descriptionDE: (value) =>
        value.length < 1
          ? t("Edesc")
          : value.length > max_entry_length
            ? t("EtooLong")
            : null,
      descriptionEN: (value) =>
        value.length < 1
          ? t("Edesc")
          : value.length > max_entry_length
            ? t("EtooLong")
            : null,
      entryType: (value) => (value == "" ? t("EentryType") : null),
      startAt: (value, values) =>
        !value && (values.endAt || values.startTimeAt) ? t("Edate") : null,
      startTimeAt: (value, values) =>
        !values.startAt && value ? t("Etime") : null,
      endAt: (value, values) =>
        !value && values.endTimeAt ? t("Edate") : null,
      endTimeAt: (value, values) =>
        !values.endAt && value ? t("Etime") : null,
      contact: (value) =>
        value == "" ||
        /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(
          value,
        )
          ? null
          : t("Email"),
    },
  });

  /*
   * Format newsletters in a value, label fashion for the Select component.
   */
  const formatNewsletters = async (
    newsletters: { id: number; sendAt: Date; isSent: boolean }[],
  ) => {
    const d: ReducedNewsletterType[] = [];
    newsletters
      .filter((n) => !n.isSent)
      .map((n) => {
        const r = new Date(n.sendAt);
        const label = dateFormatter(locale || "en", r);
        d.push({ value: String(n.id), label: label, sendAt: r });
      });
    setNewsletters(d);
  };

  const handleSubmit = async (props: addForm) => {
    const res = await addEntry({
      variables: {
        ...props,
        newsletterId: Number(props.newsletterId),
        includeStartTime: props.startTimeAt ? true : false,
        includeEndTime: props.endTimeAt ? true : false,
        isFeatured: false,
        isChecked: false,
        isDisabled: isTooLate(),
      },
    });
    if (res.data.addEntry) {
      return router.push("/");
    }
    notifications.show({
      title: t("unableAddNot"),
      message: t("unableAddNotText"),
      icon: <IconX />,
      color: "red",
    });
  };

  useEffect(() => {
    form.setFieldValue("newsletterId", id);
  }, [newsletters, id]);

  const isTooLate = () => {
    if (!form.values.newsletterId) return false;
    if (!newsletters.length) return false;

    const id = form.values.newsletterId;
    const nl = newsletters.filter(
      (n: ReducedNewsletterType) => String(n.value) == id,
    )[0];

    return (
      dayjs(new Date()) > dayjs(nl.sendAt).subtract(correction_time, "hour")
    );
  };

  const organization_types = config?.newsletter?.organization_types || {
    en: [],
    de: [],
  };
  const orgTypes = organization_types["en"].map((ot: string, i: number) => {
    return {
      value: config?.newsletter.organization_types.de[i] || "",
      label: config?.newsletter.organization_types[locale || "en"][i] || "",
    };
  });

  const entry_types = config?.newsletter?.entry_types || { en: [], de: [] };
  const entryTypes = entry_types["en"].map((et: string, i: number) => {
    return {
      value: config?.newsletter.entry_types.de[i] || "",
      label: config?.newsletter.entry_types[locale || "en"][i] || "",
    };
  });

  const getGuidelines = (
    locale: string,
    guidelines: { [key: string]: string[] },
  ) => (
    <ul className="list-disc list-outside ml-4">
      {guidelines[locale].map((line, i) => (
        <li key={i}>{line}</li>
      ))}
    </ul>
  );

  return (
    <>
      <div className="grid grid-cols-1 md:grid-cols-3 gap-4">
        <div className="col-span-1 md:col-span-2">
          <h1 className="font-black text-3xl mb-8">{t("title")}</h1>

          {isTooLate() && (
            <Alert
              title={t("deadlineOver")}
              color="red"
              icon={<IconClock />}
              variant="filled"
              className="mb-5"
            >
              {t("deadlineOverDesc")}
            </Alert>
          )}

          <form onSubmit={form.onSubmit(handleSubmit)}>
            <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
              <div className="col-span-1 md:col-span-2">
                <Select
                  label={t("newsletter")}
                  placeholder="01.01.2100"
                  data={newsletters as any}
                  {...form.getInputProps("newsletterId")}
                  withAsterisk
                />
              </div>
              <div>
                <TextInput
                  label={t("name")}
                  placeholder="Maxime Musterfrau"
                  {...form.getInputProps("author")}
                  withAsterisk
                />
              </div>
              <div>
                <TextInput
                  label={t("e-mail")}
                  placeholder="maxime.musterfrau@gmx.ch"
                  leftSection={<IconMail color={getTextColor(theme)} />}
                  {...form.getInputProps("authorMail")}
                  withAsterisk
                />
              </div>
              <div>
                <TextInput
                  label={t("org")}
                  placeholder="Studi Lerngruppe XY"
                  {...form.getInputProps("organization")}
                  withAsterisk
                />
              </div>
              <div>
                <Select
                  label={t("orgType")}
                  placeholder="Kommission"
                  data={orgTypes}
                  {...form.getInputProps("organizationType")}
                  withAsterisk
                />
              </div>
              <div>
                <TextInput
                  label={t("titleDE")}
                  placeholder="Vorstände Gesucht!"
                  {...form.getInputProps("titleDE")}
                  withAsterisk
                />
              </div>
              <div>
                <TextInput
                  label={t("titleEN")}
                  placeholder="Board Members Wanted!"
                  {...form.getInputProps("titleEN")}
                  withAsterisk
                />
              </div>
              <div>
                <Textarea
                  label={
                    <>
                      <p>
                        {t("descDE")}{" "}
                        {config?.newsletter?.guidelines && (
                          <Tooltip
                            label={getGuidelines(
                              locale || "en",
                              config.newsletter.guidelines,
                            )}
                          >
                            <span>
                              ---{" "}
                              <span style={{ textDecoration: "underline" }}>
                                {t("guidelines")}
                              </span>
                            </span>
                          </Tooltip>
                        )}
                      </p>
                      <span style={{ color: "gray" }}>
                        {form.values.descriptionDE.length} / {max_entry_length}
                      </span>
                    </>
                  }
                  maxRows={5}
                  minRows={5}
                  autosize
                  placeholder="Unsere Kommission sucht aktuell..."
                  {...form.getInputProps("descriptionDE")}
                  withAsterisk
                />
              </div>
              <div>
                <Textarea
                  label={
                    <>
                      <p>
                        {t("descEN")}{" "}
                        {config?.newsletter?.guidelines && (
                          <Tooltip
                            label={getGuidelines(
                              locale || "en",
                              config.newsletter.guidelines,
                            )}
                          >
                            <span>
                              ---{" "}
                              <span style={{ textDecoration: "underline" }}>
                                {t("guidelines")}
                              </span>
                            </span>
                          </Tooltip>
                        )}
                      </p>
                      <span style={{ color: "gray" }}>
                        {form.values.descriptionEN.length} / {max_entry_length}
                      </span>
                    </>
                  }
                  maxRows={5}
                  minRows={5}
                  autosize
                  placeholder="Our committee is currently looking for..."
                  {...form.getInputProps("descriptionEN")}
                  withAsterisk
                />
              </div>
              <div>
                <Select
                  label={t("entryType")}
                  placeholder="Other"
                  data={entryTypes}
                  {...form.getInputProps("entryType")}
                  withAsterisk
                />
              </div>
              <div>
                <TextInput
                  label={t("place")}
                  placeholder="CAB E 15.2"
                  leftSection={<IconLocation color={getTextColor(theme)} />}
                  {...form.getInputProps("place")}
                />
              </div>

              <div>
                <div className="flex gap-4">
                  <DatePickerInput
                    label={t("startDate")}
                    placeholder="01.01.2100"
                    leftSection={<IconCalendar color={getTextColor(theme)} />}
                    className="basis-4/6"
                    {...form.getInputProps("startAt")}
                  />
                  <TimeInput
                    label={t("startTime")}
                    leftSection={<IconClock color={getTextColor(theme)} />}
                    className="basis-2/6"
                    {...form.getInputProps("startTimeAt")}
                  />
                </div>
              </div>
              <div>
                <div className="flex gap-4">
                  <DatePickerInput
                    label={t("endDate")}
                    placeholder="01.01.2100"
                    leftSection={<IconCalendar color={getTextColor(theme)} />}
                    className="basis-4/6"
                    locale={locale}
                    {...form.getInputProps("endAt")}
                  />
                  <TimeInput
                    label={t("endTime")}
                    leftSection={<IconClock color={getTextColor(theme)} />}
                    className="basis-2/6"
                    {...form.getInputProps("endTimeAt")}
                  />
                </div>
              </div>

              <div>
                <TextInput
                  label={t("signUp")}
                  placeholder="https://bit.ly/ABCXYZ"
                  leftSection={<IconLink color={getTextColor(theme)} />}
                  rightSection={
                    <Tooltip label={t("signUpTT")}>
                      <ActionIcon variant="transparent">
                        <IconInfoCircle color={getTextColor(theme)} />
                      </ActionIcon>
                    </Tooltip>
                  }
                  {...form.getInputProps("signUp")}
                />
              </div>
              <div>
                <TextInput
                  label={t("website")}
                  placeholder="https://blubb.ethz.ch"
                  leftSection={<IconLink color={getTextColor(theme)} />}
                  rightSection={
                    <Tooltip label={t("websiteTT")}>
                      <ActionIcon variant="transparent">
                        <IconInfoCircle color={getTextColor(theme)} />
                      </ActionIcon>
                    </Tooltip>
                  }
                  {...form.getInputProps("website")}
                />
              </div>
              <div>
                <TextInput
                  label={t("contactUs")}
                  placeholder="contact@blubb.ethz.ch"
                  leftSection={<IconMail color={getTextColor(theme)} />}
                  rightSection={
                    <Tooltip label={t("contactusTT")}>
                      <ActionIcon variant="transparent">
                        <IconInfoCircle color={getTextColor(theme)} />
                      </ActionIcon>
                    </Tooltip>
                  }
                  {...form.getInputProps("contact")}
                />
              </div>

              <div className="col-span-1 md:col-span-2">
                <Button type="submit" mt="sm">
                  {t("submit")}
                </Button>
              </div>
            </div>
          </form>
        </div>
        <div>
          <h2 className="font-black text-xl mb-8">{t("preview")}</h2>

          <EntryPreview data={form.values} locale="de" />
          <EntryPreview data={form.values} locale="en" />
        </div>
      </div>
    </>
  );
}
