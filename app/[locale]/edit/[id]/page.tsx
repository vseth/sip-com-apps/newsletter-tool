"use client";

import { useState, useEffect } from "react";

import { useRouter } from "next/navigation";

import { useScopedI18n } from "@/locales/client";

import { Progress, Tooltip, useMantineTheme } from "@mantine/core";

import EditCard from "@/components/editCard";
import EditModal from "@/components/editModal";

import type { NewsletterType, EntryType } from "@/interfaces/newsletter";
import type { ProgressType } from "@/interfaces/edit";

import { isTooLate } from "@/utilities/datetimeFormatter";
import { getPrimaryColor } from "@/utilities/colors";

import useConfig from "@/config/useConfig";

import { gql, useQuery } from "@apollo/client";

const getNewsletterQuery = gql`
  query getNewsletter($id: Int!) {
    getNewsletter(id: $id) {
      id
      sendAt
      entries {
        id
        titleDE
        titleEN
        author
        authorMail
        descriptionDE
        descriptionEN
        entryType
        organization
        organizationType
        submittedAt
        startAt
        includeStartTime
        endAt
        includeEndTime
        place
        signUp
        website
        contact
        isFeatured
        isDisabled
        isChecked
      }
    }
  }
`;

export default function EditNewsletter({
  params: { locale, id },
}: {
  params: { locale: "de" | "en"; id: string };
}) {
  const [progress, setProgress] = useState<ProgressType[]>([]);
  const [entry, setEntry] = useState<number>(-1);

  const theme = useMantineTheme();

  const t = useScopedI18n("edit");
  const tf = useScopedI18n("form");

  const router = useRouter();
  const config = useConfig();

  const { data: newsletter, refetch } = useQuery(getNewsletterQuery, {
    variables: {
      id: Number(id),
    },
  });

  useEffect(() => {
    if (!newsletter?.getNewsletter.entries) return;
    calculateProgress(newsletter.getNewsletter);
  }, [newsletter, config]);

  const organization_types = config?.newsletter?.organization_types || {
    en: [],
    de: [],
  };
  const orgTypes = organization_types["en"].map((ot: string, i: number) => {
    return {
      value: config?.newsletter.organization_types.de[i] || "",
      label: config?.newsletter.organization_types[locale || "en"][i] || "",
    };
  });

  const entry_types = config?.newsletter?.entry_types || { en: [], de: [] };
  const entryTypes = entry_types["en"].map((et: string, i: number) => {
    return {
      value: config?.newsletter.entry_types.de[i] || "",
      label: config?.newsletter.entry_types[locale || "en"][i] || "",
    };
  });

  function rgbToHex(red: number, green: number, blue: number) {
    const rgb = (red << 16) | (green << 8) | (blue << 0);
    return "#" + (0x1000000 + rgb).toString(16).slice(1);
  }

  function hexToRgb(hex: string) {
    const normal = hex.match(/^#([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$/i);
    if (normal) return normal.slice(1).map((e) => parseInt(e, 16));

    const shorthand = hex.match(/^#([0-9a-f])([0-9a-f])([0-9a-f])$/i);
    if (shorthand) return shorthand.slice(1).map((e) => 0x11 * parseInt(e, 16));

    return null;
  }

  const calculateProgress = (newsletter: NewsletterType) => {
    const arr: ProgressType[] = [];
    let total = 0;
    const mainColor = getPrimaryColor(theme, "light");

    // interpolate colors in a weird way
    const RGB = hexToRgb(mainColor) || [];
    const entry_types = config?.newsletter?.entry_types || { de: [], en: [] };
    const colors = entry_types["de"].map((et: string, i: number) => {
      const max_rgb = 200;
      const newColorRgb = RGB.map((val) => {
        if (val > max_rgb) return val;
        return (
          ((max_rgb - val) /
            (entry_types["de"].length <= 1
              ? 1
              : entry_types["de"].length - 1)) *
            i +
          val
        );
      });
      return rgbToHex(newColorRgb[0], newColorRgb[1], newColorRgb[2]);
    });

    newsletter.entries
      .filter((e: EntryType) => !e.isDisabled)
      .forEach(() => {
        total += 1;
      });
    entryTypes.forEach((et: { value: string; label: string }, i: number) => {
      let cnt = 0;
      let max = 0;
      newsletter.entries
        .filter((e: EntryType) => e.entryType == et.value)
        .forEach((e: EntryType) => {
          if (!e.isDisabled) max += 1;
          if (!e.isDisabled && e.isChecked) cnt += 1;
        });
      const obj: ProgressType = {
        value: (cnt * 100) / total,
        color: colors[i],
        label: et.label,
        tooltip: et.label + ": " + cnt + " / " + max,
      };
      arr.push(obj);
    });
    setProgress(arr);
  };

  return (
    <>
      <h1 className="font-bold text-3xl">{t("Title")}</h1>
      {progress.length > 1 && (
        <Progress.Root radius="xl" size={24} className="my-5">
          {progress.map((s, i) => (
            <Tooltip label={s.tooltip} key={i}>
              <Progress.Section value={s.value} color={s.color}>
                <Progress.Label>{s.label}</Progress.Label>
              </Progress.Section>
            </Tooltip>
          ))}
        </Progress.Root>
      )}
      {entryTypes.map((et: { value: string; label: string }) => (
        <div key={et.value}>
          <h2 className="font-black text-2xl my-2">{et.label}</h2>
          {newsletter &&
            newsletter.getNewsletter.entries
              .filter((entry: EntryType) => entry.entryType == et.value)
              .map((entry: EntryType) => (
                <EditCard
                  entry={entry}
                  orgTypes={orgTypes}
                  isTooLate={isTooLate(
                    entry.submittedAt,
                    newsletter.getNewsletter.sendAt,
                    config?.newsletter.correction_time || 0,
                  )}
                  edit={(id: number) => {
                    setEntry(id);
                  }}
                  key={entry.id}
                />
              ))}
        </div>
      ))}
      {newsletter && (
        <EditModal
          data={
            entry >= 0
              ? newsletter.getNewsletter.entries.filter(
                  (e: EntryType) => e.id == entry,
                )[0]
              : null
          }
          newsletterId={id}
          open={entry >= 0}
          close={() => setEntry(-1)}
          refetch={refetch}
        />
      )}
    </>
  );
}
