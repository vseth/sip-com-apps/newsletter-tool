"use client";

import { useRouter } from "next/navigation";

import { useState, useEffect } from "react";

import { notifications } from "@mantine/notifications";

import dayjs from "dayjs";

import { useScopedI18n } from "@/locales/client";

import {
  Alert,
  Button,
  Card,
  Grid,
  Group,
  Modal,
  Select,
  Space,
  TextInput,
  useMantineTheme,
} from "@mantine/core";

import {
  IconMail,
  IconX,
  IconCheck,
  IconAlertTriangle,
} from "@tabler/icons-react";

import { RichTextEditor, Link } from "@mantine/tiptap";
import { useEditor } from "@tiptap/react";
import StarterKit from "@tiptap/starter-kit";
import Underline from "@tiptap/extension-underline";

import {
  dateFormatter,
  datetimeFormatter,
} from "@/utilities/datetimeFormatter";

import useConfig from "@/config/useConfig";

type ReducedNewsletterType = {
  value: string;
  label: string;
};

import { gql, useQuery, useMutation } from "@apollo/client";

const getNewslettersQuery = gql`
  query {
    getNewsletters {
      id
      isSent
      sendAt
    }
  }
`;

const getNewsletterQuery = gql`
  query getNewsletter($id: Int!) {
    getNewsletter(id: $id) {
      id
      isSent
      sendAt
    }
  }
`;

const getUncheckedEntriesQuery = gql`
  query getUncheckedEntries($id: Int!) {
    getUncheckedEntries(id: $id)
  }
`;

const sendReminderMut = gql`
  mutation sendReminder($html: String!, $receivers: [String]!) {
    sendReminder(html: $html, receivers: $receivers)
  }
`;

const sendNewsletterMut = gql`
  mutation sendNewsletter($id: Int!, $receivers: [String]!) {
    sendNewsletter(id: $id, receivers: $receivers)
  }
`;

const updateSentStatusMut = gql`
  mutation updateSentStatus($id: Int!, $status: Boolean!) {
    updateSentStatus(id: $id, status: $status)
  }
`;

export default function SendNewsletter({
  params: { locale, id },
}: {
  params: { locale: string; id: string };
}) {
  const [newslettersSelect, setNewslettersSelect] = useState<
    ReducedNewsletterType[]
  >([]);
  const [sendConfirmModalOpen, setSendConfirmModalOpen] = useState(false);
  const [reminderRec, setReminderRec] = useState<string>("");
  const [rec, setRec] = useState<string>("");

  const router = useRouter();

  const theme = useMantineTheme();
  const config = useConfig();

  const content = "";

  const editor = useEditor({
    extensions: [StarterKit, Underline, Link],
    content,
  });

  const { data: newsletters } = useQuery(getNewslettersQuery);
  const { data: newsletter } = useQuery(getNewsletterQuery, {
    variables: {
      id: Number(id),
    },
    onCompleted: (data) => {
      generateContent(data.getNewsletter);
    },
  });
  const { data: uncheckedEntries } = useQuery(getUncheckedEntriesQuery, {
    variables: {
      id: Number(id),
    },
  });
  const [sendReminder] = useMutation(sendReminderMut);
  const [sendNewsletter] = useMutation(sendNewsletterMut);
  const [updateSentStatus] = useMutation(updateSentStatusMut);

  const t = useScopedI18n("send");

  const getReleaseDate = (locale: string, sendAt: Date) => {
    const sendAtStr = dateFormatter(locale, sendAt);
    return "<b>" + sendAtStr + "</b>";
  };

  const getDeadline = (locale: string, sendAt: Date) => {
    const deadline = dayjs(new Date(sendAt))
      .subtract(config?.newsletter?.correction_time || 48, "hour")
      .subtract(1, "minute");
    return "<b>" + datetimeFormatter(locale, deadline.toDate()) + "</b>";
  };

  const formatNewsletters = () => {
    if (!newsletters) return;
    setNewslettersSelect(
      newsletters.getNewsletters
        .filter((n: { id: number; isSent: boolean; sendAt: Date }) => !n.isSent)
        .map((newsletter: { id: number; isSent: boolean; sendAt: Date }) => {
          return {
            value: String(newsletter.id),
            label: dateFormatter(locale || "en", newsletter.sendAt),
          };
        }),
    );
  };

  const generateContent = (newsletter: {
    id: number;
    isSent: boolean;
    sendAt: Date;
  }) => {
    const url =
      (config?.general?.url || "https://newsletter.vseth.ethz.ch") +
      "/add/" +
      newsletter.id;
    const link = '<a href="' + url + '">' + url + "</a>";

    const reminder_text = config?.send?.reminder_text || "";
    const cont = reminder_text
      .replace("__DATE__", getReleaseDate("de", newsletter.sendAt))
      .replace("__DATE_EN__", getReleaseDate("en", newsletter.sendAt))
      .replace("__DEADLINE__", getDeadline("de", newsletter.sendAt))
      .replace("__DEADLINE_EN__", getDeadline("en", newsletter.sendAt))
      .replace("__LINK__", link)
      .replace("__LINK__", link);
    editor!.commands.setContent(cont);
  };

  useEffect(() => {
    formatNewsletters();
  }, [newsletters, locale]);

  const send = async (receivers: string[], isTest: boolean) => {
    if (receivers.length == 0 || receivers[0].replace(" ", "") == "") {
      notifications.show({
        title: t("unableNewsletterNot"),
        message: t("enterRec"),
        icon: <IconX />,
        color: "red",
      });
      return;
    }
    await sendNewsletter({
      variables: {
        id: Number(id),
        receivers: receivers,
      },
    });
    if (!isTest) {
      updateSentStatus({
        variables: {
          id: Number(id),
          status: true,
        },
      });
    }
    notifications.show({
      title: t("newsletterSentNot"),
      message: "",
      color: "green",
      icon: <IconCheck />,
      onClose: () => {
        if (!isTest) router.push("/");
      },
    });

    return;
  };

  const sendReminderFunc = async (receivers: string[]) => {
    if (receivers.length == 0 || receivers[0].replace(" ", "") == "") {
      notifications.show({
        title: t("unableNewsletterNot"),
        message: t("enterRec"),
        icon: <IconX />,
        color: "red",
      });
      return;
    }
    await sendReminder({
      variables: {
        html: editor!.getHTML(),
        receivers: receivers,
      },
    });
    notifications.show({
      title: t("reminderSentNot"),
      message: "",
      color: "green",
      icon: <IconCheck />,
    });

    return;
  };

  const receivers = config?.send?.receivers || ["kommunikation@vseth.ethz.ch"];
  const reminder_receivers = config?.send?.reminder_receivers || [
    "kommunikation@vseth.ethz.ch",
  ];

  return (
    <>
      {newsletter && newsletters && (
        <>
          <h1 className="font-black text-3xl">{t("title")}</h1>

          <Select
            label={t("newsletter")}
            placeholder="01.01.2100"
            data={newslettersSelect}
            value={String(newsletter.getNewsletter.id)}
            onChange={(id) => {
              router.push("/send/" + id);
            }}
          />
          <Space h="xl" />

          {/* TEST */}
          <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
            <div className="flex">
              <Card
                shadow="md"
                style={{
                  border:
                    "1px solid " +
                    theme.colors[theme.primaryColor][
                      theme.primaryShade as number
                    ],
                  display: "flex",
                  justifyContent: "space-between",
                  flexDirection: "column",
                  width: "100%",
                }}
              >
                <div>
                  <h2 className="font-black text-xl">{t("newsletter")}</h2>
                  <Button
                    rightSection={<IconMail />}
                    onClick={() => setSendConfirmModalOpen(true)}
                    fullWidth
                  >
                    {t("sendButton")}
                  </Button>
                  <Space h="md" />
                  {t("currentlyConfigured")}
                  <ul className="list-disc list-outside ml-4">
                    {receivers.map((rc: string, i: number) => (
                      <li key={i}>{rc}</li>
                    ))}
                  </ul>
                  <Space h="md" />
                  {uncheckedEntries?.getUncheckedEntries && (
                    <>
                      <Alert
                        color="red"
                        icon={<IconAlertTriangle />}
                        variant="filled"
                        title={t("uncheckedEntriesTitle")}
                      >
                        {t("uncheckedEntries")}
                      </Alert>
                      <Space h="md" />
                    </>
                  )}
                </div>
                <Group align="flex-end">
                  <TextInput
                    placeholder={t("receivers")!}
                    value={rec}
                    onChange={(e) => setRec(e.target.value)}
                    leftSection={<IconMail />}
                    className="grow"
                  />
                  <Button
                    onClick={() => send(rec.split(","), true)}
                    variant="light"
                  >
                    {t("sendTestButton")}
                  </Button>
                </Group>
              </Card>
            </div>
            <div className="flex">
              <Card
                shadow="md"
                style={{
                  border:
                    "1px solid " +
                    theme.colors[theme.primaryColor][
                      theme.primaryShade as number
                    ],
                  display: "flex",
                  justifyContent: "space-between",
                  flexDirection: "column",
                  width: "100%",
                }}
              >
                <div>
                  <h2 className="font-black text-xl">{t("reminder")}</h2>
                  <Button
                    rightSection={<IconMail />}
                    onClick={() => sendReminderFunc(reminder_receivers)}
                    fullWidth
                  >
                    {t("sendRemButton")}
                  </Button>
                  <Space h="md" />
                  {t("currentlyConfigured")}
                  <ul className="list-disc list-outside ml-4">
                    {reminder_receivers.map((rc: string, i: number) => (
                      <li key={i}>{rc}</li>
                    ))}
                  </ul>
                  <Space h="md" />
                </div>
                <Group align="flex-end">
                  <TextInput
                    placeholder={t("receivers")!}
                    value={reminderRec}
                    onChange={(e) => setReminderRec(e.target.value)}
                    leftSection={<IconMail />}
                    className="grow"
                  />
                  <Button
                    onClick={() => sendReminderFunc(reminderRec.split(","))}
                    variant="light"
                  >
                    {t("sendTestRemButton")}
                  </Button>
                </Group>
              </Card>
            </div>
          </div>
          <Space h="xl" />
          {/* TEST END */}

          {editor && (
            <RichTextEditor editor={editor}>
              <RichTextEditor.Toolbar sticky>
                <RichTextEditor.ControlsGroup>
                  <RichTextEditor.Bold />
                  <RichTextEditor.Italic />
                  <RichTextEditor.Underline />
                  <RichTextEditor.Strikethrough />
                  <RichTextEditor.ClearFormatting />
                  <RichTextEditor.Code />
                </RichTextEditor.ControlsGroup>

                <RichTextEditor.ControlsGroup>
                  <RichTextEditor.H1 />
                  <RichTextEditor.H2 />
                  <RichTextEditor.H3 />
                  <RichTextEditor.H4 />
                </RichTextEditor.ControlsGroup>

                <RichTextEditor.ControlsGroup>
                  <RichTextEditor.Blockquote />
                  <RichTextEditor.Hr />
                  <RichTextEditor.BulletList />
                  <RichTextEditor.OrderedList />
                </RichTextEditor.ControlsGroup>

                <RichTextEditor.ControlsGroup>
                  <RichTextEditor.Link />
                  <RichTextEditor.Unlink />
                </RichTextEditor.ControlsGroup>
              </RichTextEditor.Toolbar>

              <RichTextEditor.Content />
            </RichTextEditor>
          )}
          <Modal
            opened={sendConfirmModalOpen}
            onClose={() => setSendConfirmModalOpen(false)}
            title="Send?"
          >
            <Button onClick={() => send(receivers, false)}>Send</Button>
          </Modal>
        </>
      )}
    </>
  );
}
