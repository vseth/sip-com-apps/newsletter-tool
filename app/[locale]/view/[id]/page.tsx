"use client";

import { useScopedI18n } from "@/locales/client";

import EntryPreview from "@/components/entryPreview";

import useConfig from "@/config/useConfig";

import { dateFormatter } from "@/utilities/datetimeFormatter";

import type { EntryType } from "@/interfaces/newsletter";

import { gql, useQuery } from "@apollo/client";

const getNewsletterQuery = gql`
  query getNewsletter($id: Int!) {
    getNewsletter(id: $id) {
      id
      sendAt
      entries {
        id
        titleDE
        titleEN
        descriptionDE
        descriptionEN
        organization
        startAt
        includeStartTime
        endAt
        includeEndTime
        place
        signUp
        website
        contact
        isFeatured
        isDisabled
      }
    }
  }
`;

export default function View({
  params: { locale, id },
}: {
  params: { locale: string; id: string };
}) {
  const t = useScopedI18n("view");

  const { data: newsletter } = useQuery(getNewsletterQuery, {
    variables: {
      id: Number(id),
    },
  });

  if (!newsletter) return <></>;

  return (
    <>
      <h1 className="font-black text-3xl">
        {t("title") +
          " " +
          dateFormatter(locale || "en", newsletter.getNewsletter.sendAt)}
      </h1>

      <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-4">
        {newsletter &&
          newsletter.getNewsletter.entries
            .filter((entry: EntryType) => !entry.isDisabled)
            .map((entry: EntryType) => (
              <div key={entry.id}>
                <EntryPreview data={entry} locale={locale || "en"} />
              </div>
            ))}
      </div>
    </>
  );
}
