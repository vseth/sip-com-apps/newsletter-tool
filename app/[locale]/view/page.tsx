"use client";

import { useRouter } from "next/navigation";

import { useScopedI18n } from "@/locales/client";

import { Alert } from "@mantine/core";

import { gql, useQuery } from "@apollo/client";

const getNextNewsletterIdQuery = gql`
  {
    getNewestNewsletterId
  }
`;

export default function ViewForward() {
  const { data: id } = useQuery(getNextNewsletterIdQuery);
  const t = useScopedI18n("common");
  const router = useRouter();

  if (id && id.getNewestNewsletterId !== 0) {
    router.push("/view/" + id.getNewestNewsletterId);
  }

  return (
    <Alert variant="filled" title={t("addNewestNewsletterTitle")}>
      {t("addNewestNewsletterText")}
    </Alert>
  );
}
