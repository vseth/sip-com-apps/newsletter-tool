"use client";

import MainCarousel from "@/components/mainCarousel";
import MainPreviews from "@/components/mainPreviews";
import useConfig from "@/config/useConfig";

export default function MainPage({
  params: { locale },
}: {
  params: { locale: "de" | "en" };
}) {
  const config = useConfig();

  return (
    <div>
      <div className="grid md:grid-cols-2 grid-cols-1 gap-8 my-24 md:my-4">
        <div className="h-full flex flex-col justify-center h-full items-start">
          <h1 className="text-5xl font-black mb-4">
            {config?.general.name || ""}
          </h1>
          <p>{config?.general.description[locale] || ""}</p>
        </div>
        <div className="hidden md:block">
          <MainCarousel />
        </div>
      </div>

      <MainPreviews />
    </div>
  );
}
