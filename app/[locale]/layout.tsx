import "@/app/globals.css";
import "@mantine/core/styles.css";
import "@mantine/dates/styles.css";
import "@mantine/notifications/styles.css";
import "@mantine/carousel/styles.css";
import "@mantine/tiptap/styles.css";

import { unstable_noStore as noStore } from "next/cache";
import type { Metadata, ResolvingMetadata } from "next";

import { Source_Sans_3 } from "next/font/google";

import { ColorSchemeScript, createTheme } from "@mantine/core";

import { getStaticParams } from "@/locales/server";
import { setStaticParamsLocale } from "next-international/server";

import I18nWrapper from "@/components/i18nWrapper";
import Navbar from "@/components/navbar";
import Footer from "@/components/footer";

import getConfig from "@/config/getConfig";

const ss3 = Source_Sans_3({
  subsets: ["latin"],
  display: "swap",
});

type Props = {
  params: { locale: "de" | "en" };
};

export async function generateMetadata(
  { params }: Props,
  parent: ResolvingMetadata,
): Promise<Metadata> {
  noStore();
  const config = await getConfig();
  const locale = params.locale || "en";

  const description = config ? config.general.description[locale] : "";

  return {
    title: config.general.name,
    description,
    metadataBase: new URL(process.env.NEXTAUTH_URL || config.general.url),
    alternates: {
      languages: {
        "en-US": "/en",
        "de-CH": "/de",
      },
    },
  };
}

export function generateStaticParams() {
  return getStaticParams();
}

type ColorType = readonly [
  string,
  string,
  string,
  string,
  string,
  string,
  string,
  string,
  string,
  string,
];

export default async function RootLayout({
  children,
  params,
}: Readonly<{
  children: React.ReactNode;
  params: { locale: "de" | "en" };
}>) {
  const locale = params?.locale || "en";

  const config = await getConfig();

  setStaticParamsLocale(locale || "en");

  const theme = createTheme({
    colors: {
      primary: config.theme.main_colors,
      red: Array(10)
        .fill(1)
        .map((e) => "#f03e3e") as unknown as ColorType,
      green: Array(10)
        .fill(1)
        .map((e) => "#40c057") as unknown as ColorType,
      yellow: Array(10)
        .fill(1)
        .map((e) => "#fab005") as unknown as ColorType,
    },
    primaryColor: "primary",
    primaryShade: {
      light: 6,
      dark: 4,
    },
  });

  return (
    <html lang={locale} className={ss3.className}>
      <head>
        <ColorSchemeScript defaultColorScheme="auto" />
        <link rel="icon" href={config.theme.favicon} sizes="any" />
        <link
          rel="apple-touch-icon"
          href={config.theme.favicon}
          type="image/png"
        />
      </head>
      <body>
        <div
          className={`flex min-h-screen flex-col justify-between bg-white dark:bg-zinc-900 dark:text-white relative ${ss3.className}`}
        >
          <Navbar locale={locale} />
          <main className="mx-auto w-screen max-w-[1300px] p-4 grow">
            <I18nWrapper locale={locale} theme={theme}>
              {children}
            </I18nWrapper>
          </main>
          <Footer />
        </div>
      </body>
    </html>
  );
}
