import { unstable_noStore as noStore } from "next/cache";
import type { Metadata } from "next";

import getConfig from "@/config/getConfig";

export async function generateMetadata(): Promise<Metadata> {
  noStore();
  const config = await getConfig();

  const description = config ? config.general.description["en"] : "";

  return {
    title: config.general.name,
    description,
    metadataBase: new URL(process.env.NEXTAUTH_URL || config.general.url),
    alternates: {
      languages: {
        "en-US": "/en",
        "de-CH": "/de",
      },
    },
  };
}

export default function RootLayout({
  children,
  params: { locale },
}: {
  children: React.ReactNode;
  params: { locale: string };
}) {
  return <>{children}</>;
}
