import { ImageResponse } from "next/og";

import getConfig from "@/config/getConfig";

// Image metadata
export const alt = "Newsletter";
export const size = {
  width: 1200,
  height: 630,
};

export const contentType = "image/png";

// Image generation
export default async function Image() {
  const config = await getConfig();

  const col = config.theme.main_colors[6];

  const ss3 = fetch(
    `${process.env.NEXTAUTH_URL || config.general.url}/fonts/SourceSans3-Black.otf`,
  ).then((res) => res.arrayBuffer());

  return new ImageResponse(
    (
      // ImageResponse JSX element
      <div
        style={{
          fontSize: 96,
          background: "white",
          width: "100%",
          height: "100%",
          display: "flex",
          flexDirection: "column",
        }}
      >
        <div
          style={{
            padding: "48px",
            display: "flex",
            flexDirection: "column",
            gap: 24,
          }}
        >
          <img
            src={config.theme.logo}
            style={{
              height: config.theme.ogLogoHeight,
              width: config.theme.ogLogoWidth,
            }}
          />
          <span>{config.general.name}</span>
        </div>
        <div
          style={{
            background: `radial-gradient(100% 100% at 100% 0px, white 4%, ${col} 6%, ${col} 14%, white 16%, white 24%, ${col} 26%, ${col} 34%, white 36%, white 44%, ${col} 46%, ${col} 54%, white 56%, white 64%, ${col} 66%, ${col} 74%, white 76%, white 84%, ${col} 86%, ${col} 94%, rgba(0, 0, 0, 0.533) 96%, rgba(0, 0, 0, 0)), radial-gradient(100% 100% at 0px 100%, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.533) 4%, ${col} 6%, ${col} 14%, white 16%, white 24%, ${col} 26%, ${col} 34%, white 36%, white 44%, ${col} 46%, ${col} 54%, white 56%, white 64%, ${col} 66%, ${col} 74%, white 76%, white 84%, ${col} 86%, ${col} 94%, white 96%)`,
            backgroundSize: "120px 120px",
            flex: 1,
            width: "100%",
          }}
        />
      </div>
    ),
    {
      ...size,
      fonts: [
        {
          name: "Source Sans 3",
          data: await ss3,
          style: "normal",
          weight: 900,
        },
      ],
    },
  );
}
