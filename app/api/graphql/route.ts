// @ts-nocheck
import { createSchema, createYoga } from "graphql-yoga";
import { typeDefs } from "@/graphql/schema";
import { resolvers } from "@/graphql/resolvers";
import { createContext } from "@/graphql/context";

import { getServerSession } from "next-auth/next";
import { getToken } from "next-auth/jwt";
import authOptions from "@/lib/authOptions";

const { handleRequest } = createYoga({
  schema: createSchema({
    typeDefs: typeDefs,
    resolvers: resolvers,
  }),

  context: async (context) => {
    const session = await getServerSession(authOptions);
    //const token = await getToken({ req: context.request });

    return {
      session,
    };
  },

  // While using Next.js file convention for routing, we need to configure Yoga to use the correct endpoint
  graphqlEndpoint: "/api/graphql",

  // Yoga needs to know how to create a valid Next response
  fetchAPI: { Response },
});

export { handleRequest as GET, handleRequest as POST };
