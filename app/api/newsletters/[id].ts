// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  const { id } = req.query;
  switch (req.method) {
    case "GET":
      if (id !== undefined) {
        prisma.newsletter
          .findUniqueOrThrow({ where: { id: +id }, include: { entries: true } })
          .then((response: any) => {
            response.entries.map((e: any) => {
              // this is information normal users should not be able to see
              delete e.author;
              delete e.authorMail;
            });
            res.status(200).json(response);
          });
      } else {
        res.status(500).json({ message: "id is undefined" });
      }
      break;
    default:
      res.setHeader("Allow", ["GET"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
  }
}
