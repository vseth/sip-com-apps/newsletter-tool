// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  switch (req.method) {
    case "GET": {
      prisma.newsletter
        .findMany({
          include: { entries: { orderBy: { organization: "asc" } } },
          orderBy: [{ sendAt: "asc" }],
        })
        .then((response: any) => {
          response.map((n: any) => {
            n.entries.map((e: any) => {
              // this is information normal users should not be able to see
              delete e.author;
              delete e.authorMail;
            });
          });
          res.status(200).json({ newsletters: response });
        });
      break;
    }
    default: {
      res.setHeader("Allow", ["GET"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
    }
  }
}
