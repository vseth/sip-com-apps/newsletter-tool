// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import nodemailer from "nodemailer";

import config from "../../../../newsletter.config";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  const transporter = nodemailer.createTransport({
    host: process.env.MAILER_HOST,
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: process.env.MAILER_USERNAME,
      pass: process.env.MAILER_PASSWORD,
    },
  });

  if (req.method == "POST") {
    const { html, rec } = req.body;

    rec
      .filter((r: string) => r.replace(" ", "") != "")
      .map(async (r: string) => {
        await transporter.sendMail({
          from: process.env.MAILER_NAME,
          to: r,
          replyTo: config.send.reply_to,
          subject: config.send.reminder_subject,
          html: html,
        });
      });

    res.status(200).json({ message: "OK" });
  }
}
