// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

async function addNewsletter(sendAt: any) {
  await prisma.newsletter.create({
    data: {
      sendAt: sendAt,
    },
  });
}

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  switch (req.method) {
    case "GET": {
      prisma.newsletter
        .findMany({ include: { entries: true } })
        .then((response: any) => {
          res.status(200).json({ newsletters: response });
        });
      break;
    }
    case "POST": {
      const { sendAt } = req.body;
      const sendAtFmt = new Date(sendAt);

      addNewsletter(sendAtFmt)
        .then(async () => {
          await prisma.$disconnect();
        })
        .catch(async (e) => {
          console.error(e);
          await prisma.$disconnect();
          process.exit(1);
        });
      res.status(200).json({ message: "OK" });
      break;
    }
    default: {
      res.setHeader("Allow", ["GET", "POST"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
    }
  }
}
