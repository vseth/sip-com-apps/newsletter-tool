// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

async function deleteNewsletter(id: number) {
  await prisma.newsletter.delete({
    where: { id: id },
  });
}

async function updateNewsletter(data: any, id: number) {
  await prisma.newsletter.update({
    where: { id: id },
    data: data,
  });
}

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  const { id } = req.query;
  switch (req.method) {
    case "GET": {
      if (id !== undefined) {
        prisma.newsletter
          .findUniqueOrThrow({ where: { id: +id }, include: { entries: true } })
          .then((response: any) => {
            res.status(200).json(response);
          });
      } else {
        res.status(500).json({ message: "id is undefined" });
      }
      break;
    }
    case "DELETE": {
      if (id !== undefined) {
        deleteNewsletter(Number(id))
          .then(async () => {
            await prisma.$disconnect();
            res.status(200).json({ message: "OK" });
          })
          .catch(async (e) => {
            await prisma.$disconnect();
            res.status(500).json({ message: e });
          });
      }
      break;
    }
    case "PATCH": {
      const data = req.body;

      if (id !== undefined) {
        updateNewsletter(data, Number(id))
          .then(async () => {
            await prisma.$disconnect();
            res.status(200).json({ message: "OK" });
          })
          .catch(async (e) => {
            await prisma.$disconnect();
            res.status(500).json({ message: e });
          });
      }
      break;
    }
    default: {
      res.setHeader("Allow", ["DELETE", "PATCH", "GET"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
    }
  }
}
