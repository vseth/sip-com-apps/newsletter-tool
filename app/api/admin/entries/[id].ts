// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

async function updateEntry(data: any, id: number) {
  await prisma.entry.update({
    where: { id: id },
    data: data,
  });
}

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  const { id } = req.query;
  switch (req.method) {
    case "PATCH": {
      const dataRaw = req.body;
      const data = {
        newsletterId: Number(dataRaw.newsletter),
        organization: dataRaw.organization,
        organizationType: dataRaw.organizationType,
        entryType: dataRaw.entryType,
        startAt: dataRaw.startAt ? new Date(dataRaw.startAt) : null,
        endAt: dataRaw.endAt ? new Date(dataRaw.endAt) : null,
        place: dataRaw.place,
        titleDE: dataRaw.titleDE,
        titleEN: dataRaw.titleEN,
        descriptionDE: dataRaw.descriptionDE,
        descriptionEN: dataRaw.descriptionEN,
        signUp: dataRaw.signUp,
        website: dataRaw.website,
        contact: dataRaw.contact,
        isFeatured: dataRaw.isFeatured,
        isChecked: dataRaw.isChecked,
        isDisabled: dataRaw.isDisabled,
      };

      updateEntry(data, Number(id))
        .then(async () => {
          await prisma.$disconnect();
        })
        .catch(async (e) => {
          console.error(e);
          await prisma.$disconnect();
          process.exit(1);
        });
      res.status(200).json({ message: "OK" });
      break;
    }
    default: {
      res.setHeader("Allow", ["PATCH"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
    }
  }
}
