// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import nodemailer from "nodemailer";

import axios from "axios";

import config from "../../../../newsletter.config";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  const { id } = req.query;

  const transporter = nodemailer.createTransport({
    host: process.env.MAILER_HOST,
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: process.env.MAILER_USERNAME,
      pass: process.env.MAILER_PASSWORD,
    },
  });

  if (req.method == "POST") {
    const url = "http://127.0.0.1:8000/renderer/" + id;
    const response = await axios.get(url);

    const { rec } = req.body;
    //res.status(200).json(response);

    rec
      .filter((r: string) => r.replace(" ", "") != "")
      .map(async (r: string) => {
        await transporter.sendMail({
          from: process.env.MAILER_NAME,
          to: r,
          replyTo: config.send.reply_to,
          subject: config.send.subject,
          html: response.data,
        });
      });

    res.status(200).json({ message: "OK" });
  }
}
