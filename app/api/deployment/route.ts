import type { NextRequest } from "next/server";
import getConfig from "next/config";
import { unstable_noStore as noStore } from 'next/cache';

export async function GET(request: NextRequest) {
  noStore();
  const { serverRuntimeConfig } = getConfig();
  const deployment = serverRuntimeConfig.deployment || "prod";

  const res = Response.json({
    deployment,
  });

  res.headers.append("Content-Type", "text/json");

  return res;
}
