import getConfig from "@/config/getConfig";
import type { NextRequest } from "next/server";

export async function GET(request: NextRequest) {
  const config = await getConfig();

  const res = Response.json(JSON.stringify(config));

  res.headers.append("Content-Type", "text/json");

  return res;
}
