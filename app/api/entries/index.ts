// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

async function addEntry(data: any) {
  await prisma.entry.create({
    data: data,
  });
}

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  switch (req.method) {
    case "POST": {
      const dataRaw = req.body;
      const data = {
        newsletterId: Number(dataRaw.newsletter),
        submittedAt: new Date(dataRaw.submittedAt),
        author: dataRaw.author,
        authorMail: dataRaw.authorMail,
        organization: dataRaw.organization,
        organizationType: dataRaw.organizationType,
        entryType: dataRaw.entryType,
        startAt: new Date(dataRaw.startAt),
        endAt: new Date(dataRaw.endAt),
        place: dataRaw.place,
        titleDE: dataRaw.titleDE,
        titleEN: dataRaw.titleEN,
        descriptionDE: dataRaw.descriptionDE,
        descriptionEN: dataRaw.descriptionEN,
        signUp: dataRaw.signUp,
        website: dataRaw.website,
        contact: dataRaw.contact,
        isFeatured: dataRaw.isFeatured,
        isChecked: dataRaw.isChecked,
        isDisabled: dataRaw.isDisabled,
      };

      addEntry(data)
        .then(async () => {
          await prisma.$disconnect();
        })
        .catch(async (e) => {
          console.error(e);
          await prisma.$disconnect();
          process.exit(1);
        });
      res.status(200).json({ message: "OK" });
      break;
    }
    default: {
      res.setHeader("Allow", ["POST"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
    }
  }
}
