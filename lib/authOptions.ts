const jwt = require("jsonwebtoken");
import KeycloakProvider from "next-auth/providers/keycloak";

const authOptions = {
  providers: [
    KeycloakProvider({
      clientId: process.env.SIP_AUTH_NEWSLETTER_CLIENT_ID || "",
      clientSecret: process.env.SIP_AUTH_NEWSLETTER_CLIENT_SECRET || "",
      issuer: process.env.SIP_AUTH_OIDC_ISSUER || "",
    }),
  ],
  callbacks: {
    jwt({ token, account }: { token: any; account: any }) {
      if (account) {
        const tok = account.access_token;
        const decoded = jwt.decode(tok, { complete: true });
        token.info = decoded;
      }
      return token;
    },
    session({ session, token }: { session: any; token: any }) {
      session.info = token.info;
      return session;
    },
  },
};

export default authOptions;
