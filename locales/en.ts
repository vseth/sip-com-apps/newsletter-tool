export default {
  nav: {
    newEntry: "New Entry",
    viewNewest: "Read Newest Newsletter",
  },
  common: {
    addNextNewsletterTitle: "No future newsletters",
    addNextNewsletterText: "Add a new newsletter in order to get forwarded.",
    addNewestNewsletterTitle: "No newest newsletters",
    addNewestNewsletterText: "Send a newsletter in order to get forwarded.",
  },
  edit: {
    Title: "Edit Newsletter",
    editEntry: "Edit Entry",
    Checked: "Checked",
    Featured: "Featured",
    TooLate: "Too Late",
    Disabled: "Disabled",
    CheckedDesc: "This entry is ready to be published",
    FeaturedDesc: "This entry will be highlighted in the newsletter",
    TooLateDesc: "This entry was handed in too late",
    DisabledDesc: "This entry will not appear on the newsletter",
    ETVSETH: "VSETH",
    ETEvents: "Events",
    ETOther: "Other",
    ETExternal: "External",
    OTVSETH: "VSETH",
    OTKommission: "Commission",
    OTFachverein: "Study Associatoin",
    OTAsOrg: "Associated Organisation",
    OTAnOrg: "Recognized Organisation",
    OTExternal: "External",
    SignUp: "Sign Up",
    Website: "Website",
    ContactUs: "Contact Us",
    Edit: "Edit",
    "E-Mail": "E-Mail",
    Contact: "Contact",
  },
  faq: {
    title: "Frequently Asked Questions",
  },
  form: {
    title: "Add Entry",
    newsletter: "Newsletter",
    name: "Name",
    "e-mail": "E-Mail",
    org: "Organisation",
    orgType: "Organisation Type",
    titleDE: "Title DE",
    titleEN: "Title EN",
    descDE: "Description DE",
    descEN: "Description EN",
    entryType: "Entry Type",
    place: "Place",
    startDate: "Start Date",
    startTime: "Start Time",
    endDate: "End Date",
    endTime: "End Time",
    signUp: "Sign Up Link",
    signUpTT: "If People should sign up, they should use this link",
    website: "Website",
    websiteTT: "If you want to link to your website, do that here",
    contactUs: "Contact E-Mail",
    contactusTT:
      "If you want people to contact you via E-Mail, enter that here",
    submit: "Submit",
    preview: "Preview",
    editTitle: "Edit Entry",
    Enewsletter: "Please select a newsletter",
    Ename: "Please Enter a Name",
    Email: "Invalid email",
    Eorg: "Please Enter an Organisation",
    EorgType: "Please Choose an Organisation Type",
    Etitle: "Please Enter a Title",
    Edesc: "Please Enter a Description",
    EtooLong: "Your Entry is too long",
    EentryType: "Please select an entry type",
    EselVSETH: "Please select VSETH as entry type",
    EnoVSETH: "Only VSETH bodies can submit VSETH type events",
    Edate: "Date cannot be empty",
    Etime: "Time cannot be empty",
    featured: "Featured",
    checked: "Checked",
    disabled: "Disabled",
    cancel: "Cancel",
    save: "Save",
    deadlineOver: "Deadline over",
    deadlineOverDesc:
      "The deadline for this newsletter is over. This means that you can submit your entry, but it might not be featured in the next newsletter.",
    guidelines: "Guidelines",
    unableAddNot: "Unknown Error",
    unableAddNotText: "This entry could not be added",
  },
  main: {
    addNewsletter: "Add Newsletter",
    add: "Add",
    newest: "Newest Newsletter",
    noNewestNewsletterTitle: "There's no newest newsletter yet",
    noNewestNewsletterText:
      "There first newsletter here will be published soon!",
    next: "Next Newsletter",
    noNextNewsletterTitle: "No next newsletter available",
    noNextNewsletterText:
      "There's no next newsletter yet. If you think that this is an error, please send an email to __EMAIL__.",
    upcoming: "Future Newsletters",
    past: "Past Newsletters",
    newsletterOf: "Newsletter of",
    addEntry: "Add Entry",
    view: "View",
    edit: "Edit",
    delete: "Delete",
    removeEntries:
      "Please deactivate or move all entries in order to delete this newsletter.",
    uSure: "Are you sure that you want to delete this newsletter",
    cancel: "Cancel",
    send: "Send",
    showMore: "Show More",
    hide: "hide",
    deadline: "Submission Deadline",
    deadlineOver: "Deadline over",
    deadlineOverDesc:
      "The deadline for this newsletter is over. This means that you can submit your entry, but it might not be featured in the next newsletter.",
    ETVSETH: "VSETH",
    ETEvents: "Events",
    ETOther: "Other",
    ETExternal: "External",
    noNewestNewsletter: "There's no newest newsletter yet",
    releaseDate: "Release Date",
    newsletterWasAdded: "Newsletter was added",
  },
  send: {
    title: "Send Newsletter",
    newsletter: "Newsletter",
    sendButton: "Send Newsletter",
    sendTestButton: "Send Test Newsletter",
    or: "or",
    sendRemButton: "Send Reminder",
    sendTestRemButton: "Send Test Reminder",
    receivers: "Receivers (comma separated)",
    reminder: "Reminder",
    showMore: "Show More",
    showLess: "Show Less",
    newsletterSentNot: "Newsletter Sent",
    reminderSentNot: "Reminder Sent",
    unableNewsletterNot: "Unable to send Newsletter",
    unableReminderNot: "Unable to send Reminder",
    error: "Error",
    enterRec: "Please enter recipients",
    currentlyConfigured: "Configured Recipients:",
    uncheckedEntries: "This newsletter still has unchecked entries.",
    uncheckedEntriesTitle: "Unchecked Entries",
  },
  view: {
    title: "Newsletter of",
  },
} as const;
