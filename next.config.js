/** @type {import('next').NextConfig} */
const nextConfig = {
  serverRuntimeConfig: {
    deployment: process.env.DEPLOYMENT || "prod",
  },
};

module.exports = nextConfig;
