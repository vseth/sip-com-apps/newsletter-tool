import config from "../newsletter.config";
import { NewsletterType } from "../interfaces/newsletter";
import { EntryType } from "../interfaces/newsletter";

export default function sort(newsletter: any) {
  const entry_types = config.newsletter?.entry_types || { en: [], de: [] };
  const organization_types = config.newsletter?.organization_types || {
    en: [],
    de: [],
  };

  const compare_func = (a: EntryType, b: EntryType) => {
    /*
     * first, sort by entry type
     * second, sort by organization type
     * third, sorty by organization
     */

    const ET_indices = [
      entry_types["de"].indexOf(a.entryType),
      entry_types["de"].indexOf(b.entryType),
    ];
    if (ET_indices[0] == -1)
      // entryType not found
      return 1; // a should go to the end
    if (ET_indices[1] == -1)
      // entryType not found
      return -1; // b should go to the end
    if (ET_indices[0] < ET_indices[1]) return -1;
    if (ET_indices[0] > ET_indices[1]) return 1;

    const OT_indices = [
      organization_types["de"].indexOf(a.organizationType),
      organization_types["de"].indexOf(b.organizationType),
    ];
    if (OT_indices[0] == -1)
      // organizationType not found
      return 1; // a should go to the end
    if (OT_indices[1] == -1)
      // organizationType not found
      return -1; // b should go to the end
    if (OT_indices[0] < OT_indices[1]) return -1;
    if (OT_indices[0] > OT_indices[1]) return 1;

    if (a.organization < b.organization) return -1;
    if (a.organization > b.organization) return 1;
    return 0;
  };

  newsletter.entries.sort(compare_func);
}
