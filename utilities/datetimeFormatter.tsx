import dayjs from "dayjs";
import type { Dayjs } from "dayjs";

const customParseFormat = require("dayjs/plugin/customParseFormat");
dayjs.extend(customParseFormat);

require("dayjs/locale/en");
require("dayjs/locale/de");

export function datetimeFormatter(locale: string, date: Date | Dayjs) {
  dayjs.locale(locale);
  const d = dayjs(date);
  return locale == "de"
    ? d.format("dd, DD.MM.YY, HH:mm")
    : d.format("ddd, DD.MM.YY, HH:mm");
}

export function longDatetimeFormatter(locale: string, date: Date): string {
  dayjs.locale(locale);
  const d = dayjs(date);
  return locale == "de"
    ? d.format("dd, DD. MMM YY, HH:mm")
    : d.format("ddd, MMM DD, YY, HH:mm");
}

export function dateFormatter(locale: string, date: Date): string {
  if (!date) return "";
  dayjs.locale(locale);
  const d = dayjs(date);
  return locale == "de" ? d.format("dd, DD.MM.YY") : d.format("ddd, DD.MM.YY");
}

export function longDateFormatter(locale: string, date: Date | string): string {
  if (!date) return "";
  dayjs.locale(locale);
  const d = dayjs(date);
  return locale === "de"
    ? d.format("dd, DD. MMM YYYY")
    : d.format("ddd, MMM DD YYYY");
}

export function timeFormatter(date: Date): string {
  const d = dayjs(date);
  return d.format("HH:mm");
}

export function stringToDatetime(date: Date | string) {
  if (!date) return undefined;
  const d = new Date(Number(date));
  return d;
}

export function stringToTimeString(date: Date | string) {
  if (!date) return undefined;
  const d = new Date(Number(date));
  const s = d
    .toLocaleDateString("en-GB", {
      // you can use undefined as first argument
      year: "numeric",
      month: "2-digit",
      day: "2-digit",
      hour: "2-digit",
      minute: "2-digit",
    })
    .split(", ")[1];
  return s;
}

export function eventTimeFormatter(
  locale: string,
  startAtStr?: string | Date | null,
  includeStartTime?: boolean,
  endAtStr?: string | Date | null,
  includeEndTime?: boolean,
  place?: string,
) {
  //if (startAtStr && startAtStr instanceof Date) return "";
  //if (endAtStr && endAtStr instanceof Date) return "";

  // @ts-ignore
  const startAt = stringToDatetime(startAtStr);
  // @ts-ignore
  const endAt = stringToDatetime(endAtStr);

  let str = "";

  // start time
  if (startAt) {
    if (includeStartTime) str += datetimeFormatter(locale, startAt);
    else str += dateFormatter(locale, startAt);
  }

  // end time
  if (startAt && endAt) {
    if (includeEndTime)
      if (startAt.getDate() == endAt.getDate())
        str += " – " + timeFormatter(endAt);
      else str += " – " + datetimeFormatter(locale, endAt);
    else if (startAt.getDate() != endAt.getDate())
      str += " – " + dateFormatter(locale, endAt);
  }

  // place
  if (place) str += " @ " + place;
  return str;
}

export function mergeTimeAndDate(date?: Date | string | null, time?: string) {
  if (!date) return null;
  if (typeof date == "string") {
    // @ts-ignore
    date = stringToDatetime(date);
  }

  return new Date(
    date!.getFullYear(),
    date!.getMonth(),
    date!.getDate(),
    time ? Number(time.split(":")[0]) : new Date().getHours(),
    time ? Number(time.split(":")[1]) : new Date().getHours(),
    time ? 0 : new Date().getSeconds(),
  );
}

export function isTooLate(
  submittedAt: Date | string | undefined,
  sendAt: Date,
  correctionTime: number,
) {
  if (!correctionTime || !submittedAt) return false;
  if (typeof submittedAt === "string")
    return (
      dayjs(new Date(Number(submittedAt))) >
      dayjs(sendAt).subtract(correctionTime, "hour")
    );
  return dayjs(submittedAt) > dayjs(sendAt).subtract(correctionTime, "hour");
}
