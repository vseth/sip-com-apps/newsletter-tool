import { useState, useEffect } from "react";
import axios from "axios";
import { useSession } from "next-auth/react";
import hasAccess from "./hasAccess";

const useAuth = (admin: boolean) => {
  const [access, setAccess] = useState<boolean>(false);

  const { data: session } = useSession();

  const getD = async () => {
    const res = await axios.get("/api/deployment");
    setAccess(hasAccess(session, admin, res.data.deployment));
  };

  useEffect(() => {
    (async () => {
      await getD();
    })();
  }, [session]);

  return access;
};

export default useAuth;
