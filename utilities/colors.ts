// @ts-nocheck

import config from "../newsletter.config";

export function getPrimaryColor(theme, colorScheme) {
  return theme.colors.primary[theme.primaryShade[colorScheme || "light"]];
}

export function getTextColor(theme) {
  if (theme.colorScheme == "light") return theme.black;
  else return theme.colors.dark[0];
}

export function getBackgroundColor(theme) {
  return theme.colors.primary[6];
}

export function getSurfaceColor(theme) {
  if (theme.colorScheme == "light") return "white";
  else return theme.colors.dark[3];
}

export function getStripeColor(theme) {
  return theme.colors.primary[5];
}
