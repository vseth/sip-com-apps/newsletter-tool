export default function hasAccess(
  session: any,
  admin: boolean,
  deployment: string = "prod",
) {
  // for a dev environment, always give the user admin
  if (process.env.DEPLOYMENT && process.env.DEPLOYMENT === "dev")
    return !!session;
  if (deployment === "dev") return !!session;

  // not logged in --> no access
  if (!session) return false;
  // if no admin is required and is logged in --> access
  if (!admin) return true;
  if (!session.info.payload.resource_access) return false;
  const r_a = session.info.payload.resource_access;

  return (
    r_a[
      Object.keys(r_a)[0] || ""
    ].roles.includes("admin")
  );
}
