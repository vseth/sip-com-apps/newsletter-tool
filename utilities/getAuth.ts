import hasAccess from "./hasAccess";

export default function getAuth(session: any, admin: boolean) {
  return hasAccess(session, admin, process.env.DEPLOYMENT || "prod");
}
