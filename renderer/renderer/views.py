'''
Convert transpiled tailwind to inline css here: https://templates.mailchimp.com/resources/inline-css/

'''

from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render

import requests
import json
import dateutil.parser
import pytz

def index(request):
    '''
    HOST = 'http://localhost:3000/api/newsletters/' + str(id)
    response = requests.get(HOST)
    json_str = response.content
    data = json.loads(json_str)
    '''
    data = json.loads(request.body)['newsletter']
    template = json.loads(request.body)['template']

    tz = pytz.timezone('CET')

    sendAt = dateutil.parser.isoparse(data['sendAt'])
    sendAt_CET = sendAt.astimezone(tz)

    possible_categories = ['VSETH', 'Events', 'Sonstiges', 'Extern']
    found_categories = []

    hasFeatured = False

    for i in range(len(data['entries'])):
        if(data['entries'][i]['isFeatured'] == True and data['entries'][i]['isDisabled'] == False):
            hasFeatured = True
        startAtRaw = data['entries'][i]['startAt']
        endAtRaw   = data['entries'][i]['endAt']
        if(startAtRaw != None):
            startAt = dateutil.parser.isoparse(startAtRaw)
            startAt_CET = startAt.astimezone(tz)
            startAt_str = startAt_CET.strftime('%a, %d.%m.%y, %H:%M')
        else:
            startAt_str = ''
        if(endAtRaw != None):
            endAt = dateutil.parser.isoparse(endAtRaw)
            endAt_CET = endAt.astimezone(tz)
            if(startAt.strftime('%Y%m%d') == endAt.strftime('%Y%m%d')): # same day
                endAt_str = endAt_CET.strftime('%H:%M')
            else:
                endAt_str = endAt_CET.strftime('%a, %d.%m.%y, %H:%M')
        else:
            endAt_str = ''

        data['entries'][i]['startAt'] = startAt_str
        data['entries'][i]['endAt'] = endAt_str

        if data['entries'][i]['entryType'] not in found_categories:
            found_categories.append(data['entries'][i]['entryType'])

    context = {
        'id': data['id'],
        'sendAt': sendAt_CET.strftime('%B %d, %Y'),
        'entries': data['entries'],
        'categories': found_categories,
        'hasFeatured': hasFeatured
    }
    return render(request, template + ".html", context)
