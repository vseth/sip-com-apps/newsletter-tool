# Newsletter

This is the new VSETH Newsletter tool. For the old one, see https://gitlab.ethz.ch/schochal/newsletter.

## User Documentation

The Newsletter-Application is split into multiple pages.

### Main Page

The Main Page allows to see all upcoming and past newsletters. For each upcoming newsletter, every user can add an entry by clicking on the "Add Entry" Button.

The Administrator has a few more options, though. They can

- Add Newsletters
- View upcoming Newsletters
- Edit the entries of upcoming Newsletters
- Send a newsletter or its reminder
- Delete a newsletter if all its entries are disabled

### Add Entry

Any user can add an entry to any upcoming newsletter here. If the deadline for a newsletter is over, the user is notified about this, and the entry will be disabled by default. A preview for the entry is also available.

The form will throw an error if

- any mandatory field is empty
- the E-Mail address does not have the form of an e-mail address
- a description is longer than the max_entry_length
- only one of orgType and entryType is VSETH or External
- only an end time/date is provided
- only a time is provided for start/end
- the contact us E-Mail address does not have the form of an e-mail address

There's a few new fields compared to the "old newsletter": 

- Sign Up: A sign up link, usually for events. It will be displayed as a button.
- Website: If people should visit an organisations website, this is where to add it. It will be displayed as a button.
- Contact Us: An email address people should use for contact. This is useful for e.g. finding new board members through the newsletter.

### Edit Newsletter

This page is only accessible for the admin user.

Here, the admin user can change any newsletter entry. On top, they can see their correction progress, as any edited entry will automatically be "checked".

There's a few parameters an entry can have:

- Checked: This entry was just edited. A newsletter is ready as soon as all entries are checked.
- Featured: This entry will be featured on top of the newsletter.
- Disabled: This entry will not appear in the final newsletter.
- Too Late: This entry was handed in too late. It has to be manually enabled.

### View Newsletter

### Send Newsletter

## Technical Documentation


## Configuration

The Application can be configured with the file `newsletter.config.js`


## Build: TODO

To run this application locally, use docker-compose or the following commands:

```
git clone TODO
cd newsletter

yarn install
yarn run dev
```

You'll see the application under http://localhost:3000.
