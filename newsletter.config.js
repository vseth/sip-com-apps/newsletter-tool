const getConfig = async () => {
  const res = await fetch(
    process.env.NEWSLETTER_CONFIG ||
      "https://static.vseth.ethz.ch/assets/vseth-0000-vseth/newsletter.json",
    {
      next: {
        revalidate: 86400,
      },
    },
  );

  return res.json();
};

export default await getConfig();
