const config: {
  general: {
    url: string;
    name: string;
  };
  newsletter: {
    max_entry_length: number;
    correction_time: number;
    entry_types: {
      de: string[];
      en: string[];
      [key: string]: string[];
    };
    organization_types: {
      de: string[];
      en: string[];
      [key: string]: string[];
    };
  };
  theme: {
    config: string;
    favicon: string;
    main_colors: string[] | null;
    dark_colors: string[] | null;
    primary_shade: number | null;
    progressbar_colors: string[] | null;
    entry_shade: {
      light: number;
      dark: number;
    };
  };
  send: {
    receivers: string[];
    reminder_receivers: string[];
    reply_to: string;
    subject: string;
    reminder_subject: string;
    reminder_text: string;
  };
} = {
  general: {
    /*
     * At which address can this tool be found?
     */
    url: "https://newsletter.vseth.ethz.ch",
    /*
     * Application Name, visible in Title and Navbar
     */
    name: "VSETH Newsletter",
  },
  newsletter: {
    /*
     * how long (in characters) should an entry be allowed to be?
     * this rule does not apply for correction
     */
    max_entry_length: 400,
    /*
     * how many hours pass between the submission deadline and the
     * release date (which is, here, e.g. monday, 01.01.2023 at 00:00)
     * 48 = friday midnight
     */
    correction_time: 48,
    /*
     * Types of newsletter entry. The table of contents will be split
     * into these categories, in this order.
     */
    entry_types: {
      de: ["VSETH", "Events", "Sonstiges", "Extern"],
      en: ["VSETH", "Events", "Other", "External"],
    },
    /*
     * Types of organizations. Entries will be sorted by entry type first,
     * by organization type second and by alphabet third.
     */
    organization_types: {
      de: [
        "VSETH",
        "Kommission",
        "Fachverein",
        "Assoziierte Organisation",
        "Anerkannte Organisation",
        "Extern",
      ],
      en: [
        "VSETH",
        "Commission",
        "Study Association",
        "Associated Organization",
        "Recognized Organization",
        "External",
      ],
    },
  },
  theme: {
    /*
     * Url of the VSETH Static config.json
     */
    config: "https://static.vseth.ethz.ch/assets/vseth-0000-vseth/config.json",
    /*
     * Favicon (Icon in Title), link to static.vseth.ethz.ch
     */
    favicon: "https://static.vseth.ethz.ch/assets/vseth-0000-vseth/favicon.ico",
    /*
     * Color Scheme of the application. Array from light to dark.
     */
    main_colors: null /*[
      '#eeeeee',
      '#d3d3d3',
      '#b9b9b9',
      '#9e9e9e',
      '#848484',
      '#696969',
      '#4f4f4f',
      '#343434',
      '#1a1a1a',
      '#000000',
    ],*/,
    dark_colors: null /*[
      '#C1C2C5',
      '#A6A7AB',
      '#909296',
      '#5c5f66',
      '#858b9a',
      '#2C2E33',
      '#36383f',
      '#28292d', // changed
      '#141517',
      '#101113',
    ],*/,
    /*
     * Index of the color from main_colors that the application should use
     * as default color. VSETH uses 7.
     */
    primary_shade: null, //9,
    /*
     * Colors of the progressbar sections in /edit
     */
    progressbar_colors: ["#009fe3", "#F03A47", "#183059", "#AF5B5B"],
    /*
     * Index of the color from main_colors that newsletter entries should use
     * as background.
     */
    entry_shade: {
      light: 0,
      dark: 6,
    },
  },
  send: {
    // who will the finished newsletter be sent to?
    /*receivers: [
      'mitglieder@lists.vseth.ethz.ch',
      'newsletter-extra@lists.vseth.ethz.ch',
    ],*/
    receivers: [
      "alexander.schoch@vseth.ethz.ch",
      "alexander.schoch@amiv.ethz.ch",
    ],
    // who will the newsletter reminder be sent to?
    /*reminder_receivers: [
      'kommissionen@lists.vseth.ethz.ch',
      'fachvereine@lists.vseth.ethz.ch',
      'studorgs@lists.vseth.ethz.ch',
    ],*/
    reminder_receivers: [
      "alexander.schoch@vseth.ethz.ch",
      "alexander.schoch@amiv.ethz.ch",
      "schochal@student.ethz.ch",
    ],
    // if people reply to any e-mail, where should it go?
    reply_to: "kommunikation@vseth.ethz.ch",
    subject: "Newsletter", // the [VSETH] is added in the mailing list
    reminder_subject: "VSETH Newsletter Reminder",
    // text of the reminder e-mail
    reminder_text: `*English below*</br>
</br>
Liebe Fachvereine, Kommissionen, assoziierte und anerkannte Organisationen,</br>
</br>
Der nächste VSETH-Newsletter wird am __DATE__ verschickt. Bitte tragt eure Beiträge bis zum __DEADLINE__ ins Newslettertool ein: __LINK__</br>
</br>
Hier die Richtlinien (bitte lesen):
<ul>
<li>Deutsche und englische Version einreichen, der Newsletter erreicht auch viele internationale Studierende</li>
<li>Bitte den deutschen Text gendern (<a href="https://www.scribbr.de/gendern/woerterbuch/">https://www.scribbr.de/gendern/woerterbuch/</a>)</li>
<li>Schreibt immer im Namen der Organisation, nie als WIR</li>
<li>Keine Wiederholungen von Ankündigungen in früheren Newslettern</li>
<li>Titel, Datum, Ort und Uhrzeit nicht vergessen</li>
<li>Nur einen Beitrag pro Fachverein / Kommission / Organisation</li>
</ul>Liebe Grüsse,</br>
Euer VSETH Kommunikationsteam</br>
</br>
___________________________________</br>
</br>
Dear study associations, commissions, associated and recognized organizations,</br>
</br>
The next VSETH-Newsletter of this year will be sent on __DATE_EN__. Please add your entry into the newsletter tool until __DEADLINE_EN__: __LINK__</br>
</br>
These are the guidelines (please read carefully):<ul>
<li>Hand in a German version as well, the newsletter will be going to a lot of German-speakers</li>
<li>Please gender the German text (<a href="https://www.scribbr.de/gendern/woerterbuch/">https://www.scribbr.de/gendern/woerterbuch/</a>)</li>
<li>Never use WE, but refer to yourself in the name of the organization</li>
<li>No repetition of announcements from past newsletters</li>
<li>Do not forget the title, date, time, location</li>
<li>Only one announcement by each organisation</li>
</ul>Kind regards,</br>
Your VSETH Communication team</br>
    `,
  },
};

// module.exports = config;
export default config;
