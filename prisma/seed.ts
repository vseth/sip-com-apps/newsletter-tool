// prisma/seed.ts
import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

async function main() {
  await prisma.newsletter.create({
    data: {
      sendAt: new Date(),
      isSent: false,
      entries: {
        create: [
          {
            submittedAt: new Date(),
            author: "Alexander Schoch",
            authorMail: "alexander.schoch@vseth.ethz.ch",
            organization: "Polyband",
            organizationType: "Anerkannte Organisation",
            entryType: "Events",
            startAt: new Date(),
            place: "GEP",
            titleDE: "Probe",
            titleEN: "Rehearsal",
            descriptionDE:
              "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a",
            descriptionEN:
              "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a",
            signUp: "https://polyband.ch",
            website: "https://polyband.ch",
            contact: "praesidum@polyband.ch",
            isChecked: false,
            isDisabled: false,
            isFeatured: false,
          },
          {
            submittedAt: new Date(),
            author: "Alexander Schoch",
            authorMail: "alexander.schoch@vseth.ethz.ch",
            organization: "Polyband",
            organizationType: "Anerkannte Organisation",
            entryType: "Events",
            startAt: new Date(),
            place: "GEP",
            titleDE: "Probe",
            titleEN: "Rehearsal",
            descriptionDE:
              "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a",
            descriptionEN:
              "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a",
            signUp: "https://polyband.ch",
            website: "https://polyband.ch",
            contact: "praesidum@polyband.ch",
            isChecked: false,
            isDisabled: false,
            isFeatured: false,
          },
        ],
      },
    },
  });
}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
