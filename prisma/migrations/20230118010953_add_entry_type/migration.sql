/*
  Warnings:

  - Added the required column `entryType` to the `Entry` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `Entry` ADD COLUMN `entryType` VARCHAR(255) NOT NULL;
