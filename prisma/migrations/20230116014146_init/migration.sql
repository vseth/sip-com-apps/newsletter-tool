-- CreateTable
CREATE TABLE `Newsletter` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `sendAt` DATETIME(3) NOT NULL,
    `isSent` BOOLEAN NOT NULL DEFAULT false,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Entry` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `newsletterId` INTEGER NOT NULL,
    `submittedAt` DATETIME(3) NOT NULL,
    `author` VARCHAR(255) NOT NULL,
    `authorMail` VARCHAR(255) NOT NULL,
    `organization` VARCHAR(255) NOT NULL,
    `organizationType` VARCHAR(255) NOT NULL,
    `startAt` DATETIME(3) NULL,
    `endAt` DATETIME(3) NULL,
    `place` VARCHAR(255) NULL,
    `titleDE` VARCHAR(255) NOT NULL,
    `titleEN` VARCHAR(255) NOT NULL,
    `descriptionDE` VARCHAR(191) NULL,
    `descriptionEN` VARCHAR(191) NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `Entry` ADD CONSTRAINT `Entry_newsletterId_fkey` FOREIGN KEY (`newsletterId`) REFERENCES `Newsletter`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
