-- AlterTable
ALTER TABLE `Entry` ADD COLUMN `includeEndTime` BOOLEAN NOT NULL DEFAULT true,
    ADD COLUMN `includeStartTime` BOOLEAN NOT NULL DEFAULT true;
