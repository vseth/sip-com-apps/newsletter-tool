-- DropForeignKey
ALTER TABLE `Entry` DROP FOREIGN KEY `Entry_newsletterId_fkey`;

-- AddForeignKey
ALTER TABLE `Entry` ADD CONSTRAINT `Entry_newsletterId_fkey` FOREIGN KEY (`newsletterId`) REFERENCES `Newsletter`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
