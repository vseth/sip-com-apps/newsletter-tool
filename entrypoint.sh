#!/bin/bash

echo "DATABASE_URL=mysql://$SIP_MYSQL_NL_USER:$SIP_MYSQL_NL_PW@$SIP_MYSQL_NL_SERVER:3306/$SIP_MYSQL_NL_NAME?schema=public" >> .env

npx prisma generate
npx prisma migrate deploy

npm run start
