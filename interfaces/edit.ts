export interface ProgressType {
  value: number;
  color: string;
  label: string;
  tooltip: string;
}
