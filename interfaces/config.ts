export interface ConfigType {
  general: {
    url: string;
    name: string;
    description: {
      de: string;
      en: string;
    };
  };
  newsletter: {
    template: string;
    max_entry_length: number;
    correction_time: number;
    entry_types: {
      de: string[];
      en: string[];
    };
    organization_types: {
      de: string[];
      en: string[];
    };
    guidelines: {
      de: string[];
      en: string[];
    };
  };
  theme: {
    config: string;
    logo: string;
    "logo-mono": string;
    favicon: string;
    main_colors: readonly [
      string,
      string,
      string,
      string,
      string,
      string,
      string,
      string,
      string,
      string,
    ];
    ogLogoWidth: string;
    ogLogoHeight: string;
  };
  send: {
    receivers: string[];
    reminder_receivers: string[];
    reply_to: string;
    subject: string;
    reminder_subject: string;
    reminder_text: string;
  };
}
