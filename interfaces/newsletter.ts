export interface NewsletterType {
  id: number;
  sendAt: string | Date;
  isSent: boolean;
  entries: EntryType[];
}

/*
 * This is different to EntryType because the isFeatured, isChecked and
 * isDisabled booleans, as well as the id, do not exist on here
 */
export interface EntryFormType {
  id: null;
  newsletterId: number;
  submittedAt: string;
  author?: string;
  authorMail?: string;
  organization: string;
  organizationType: string;
  entryType: string;
  startAt: string;
  startTimeAt: string;
  endAt: string;
  endTimeAt: string;
  place: string;
  titleDE: string;
  titleEN: string;
  descriptionDE: string;
  descriptionEN: string;
  signUp: string;
  website: string;
  contact: string;
}

export interface EntryType {
  id?: number;
  newsletterId?: number | string | null;
  submittedAt?: string;
  author?: string;
  authorMail?: string;
  organization: string;
  organizationType: string;
  entryType: string;
  startAt?: Date | string | null;
  startTimeAt?: string;
  includeStartTime?: boolean;
  endAt?: Date | string | null;
  endTimeAt?: string;
  includeEndTime?: boolean;
  place: string;
  titleDE: string;
  titleEN: string;
  descriptionDE: string;
  descriptionEN: string;
  signUp: string;
  website: string;
  contact: string;
  isFeatured?: boolean;
  isChecked?: boolean;
  isDisabled?: boolean;
}
