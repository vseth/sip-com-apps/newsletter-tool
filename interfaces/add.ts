export interface addForm {
  newsletterId: string | null;
  author: string;
  authorMail: string;
  organization: string;
  organizationType: string;
  titleDE: string;
  titleEN: string;
  descriptionDE: string;
  descriptionEN: string;
  entryType: string;
  place: string;
  startAt: Date | null;
  startTimeAt: string;
  endAt: Date | null;
  endTimeAt: string;
  signUp: string;
  website: string;
  contact: string;
}
