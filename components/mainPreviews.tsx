"use client";

import { useState } from "react";

import { useScopedI18n } from "@/locales/client";

import { useSession } from "next-auth/react";

import dayjs from "dayjs";

import { Accordion, Alert, Button, Center, Space } from "@mantine/core";

import { IconInfoCircle, IconPlus } from "@tabler/icons-react";

import NewsletterCard from "@/components/newsletterCard";
import AddNewsletterModal from "@/components/addNewsletterModal";
import useConfig from "@/config/useConfig";

import useAuth from "@/utilities/useAuth";

import { gql, useQuery } from "@apollo/client";

import type { NewsletterType } from "@/interfaces/newsletter";

const getNewslettersQuery = gql`
  query {
    getNewsletters {
      id
      isSent
      sendAt
      entries {
        id
        titleDE
        titleEN
        organization
        entryType
      }
    }
  }
`;

export default function MainPreviews() {
  const [newest, setNewest] = useState<NewsletterType | null>(null);
  const [next, setNext] = useState<NewsletterType | null>(null);
  const [open, setOpen] = useState<boolean>(false);

  const { data: newsletters, refetch } = useQuery(getNewslettersQuery, {
    onCompleted: (data) => {
      getNextAndNewest(data.getNewsletters);
    },
  });

  const { data: session } = useSession();
  const config = useConfig();
  const t = useScopedI18n("main");
  const access = useAuth(true);

  const noNextNewsletterTexts = t("noNextNewsletterText").split("__EMAIL__");
  const contact_email = config?.send?.reply_to || "kommunikation@vseth.ethz.ch";

  const getNextAndNewest = (newsletters: NewsletterType[]) => {
    let next: NewsletterType | null = null;
    let newest: NewsletterType | null = null;

    newsletters
      .filter((n: NewsletterType) => !n.isSent)
      .map((newsletter: NewsletterType) => {
        const sendAt = dayjs(newsletter.sendAt);
        if (!next) {
          next = newsletter;
        } else if (sendAt < dayjs(next.sendAt)) {
          next = newsletter;
        }
      });

    newsletters
      .filter((n: NewsletterType) => n.isSent)
      .map((newsletter: NewsletterType) => {
        const sendAt = dayjs(newsletter.sendAt);
        if (!newest) {
          newest = newsletter;
        } else if (sendAt > dayjs(newest.sendAt)) {
          newest = newsletter;
        }
      });

    setNext(next);
    setNewest(newest);
  };

  const futureNewsletters =
    newsletters?.getNewsletters.filter(
      (n: NewsletterType) => !n.isSent && (!next || n.id != next.id),
    ) || [];
  const pastNewsletters =
    newsletters?.getNewsletters.filter(
      (n: NewsletterType) => n.isSent && (!newest || n.id != newest.id),
    ) || [];

  return (
    <div>
      {access && (
        <Center className="my-8">
          <Button
            size="lg"
            leftSection={<IconPlus />}
            onClick={() => setOpen(true)}
          >
            {t("addNewsletter")}
          </Button>
        </Center>
      )}
      <AddNewsletterModal open={open} setOpen={setOpen} refetch={refetch} />

      <h2 className="font-black text-2xl">{t("next")}</h2>
      {next ? (
        <NewsletterCard
          newsletter={next}
          largeTOC={true}
          session={session}
          refetch={refetch}
        />
      ) : (
        <Alert
          icon={<IconInfoCircle className="text-white" />}
          title={t("noNextNewsletterTitle")}
          variant="filled"
          className="text-white"
        >
          {noNextNewsletterTexts[0]}
          <a className="underline" href={"mailto:" + contact_email}>
            {contact_email}
          </a>
          {noNextNewsletterTexts[1] || ""}
        </Alert>
      )}
      <Space h="xl" />

      <h2 className="font-black text-2xl">{t("newest")}</h2>
      {newest ? (
        <NewsletterCard
          newsletter={newest}
          largeTOC={true}
          session={session}
          refetch={refetch}
        />
      ) : (
        <Alert
          icon={<IconInfoCircle className="text-white" />}
          title={t("noNewestNewsletterTitle")}
          variant="filled"
          className="text-white"
        >
          {t("noNewestNewsletterText")}
        </Alert>
      )}
      <Space h="xl" />

      <Accordion variant="separated">
        {futureNewsletters.length > 0 && (
          <Accordion.Item value="upcoming">
            <Accordion.Control>
              <h3>{t("upcoming")}</h3>
            </Accordion.Control>
            <Accordion.Panel>
              <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-4">
                {newsletters &&
                  futureNewsletters.map((newsletter: NewsletterType) => (
                    <div className="flex" key={newsletter.id}>
                      <NewsletterCard
                        newsletter={newsletter}
                        key={newsletter.id}
                        largeTOC={false}
                        session={session}
                        refetch={refetch}
                      />
                    </div>
                  ))}
              </div>
            </Accordion.Panel>
          </Accordion.Item>
        )}
        {pastNewsletters.length > 0 && (
          <Accordion.Item value="past">
            <Accordion.Control>
              <h3>{t("past")}</h3>
            </Accordion.Control>
            <Accordion.Panel>
              <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-4">
                {newsletters &&
                  pastNewsletters.map((newsletter: NewsletterType) => (
                    <div className="flex" key={newsletter.id}>
                      <NewsletterCard
                        newsletter={newsletter}
                        key={newsletter.id}
                        largeTOC={false}
                        session={session}
                        refetch={refetch}
                      />
                    </div>
                  ))}
              </div>
            </Accordion.Panel>
          </Accordion.Item>
        )}
      </Accordion>
    </div>
  );
}
