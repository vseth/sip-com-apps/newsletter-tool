import getCanineConfig from "@/config/getCanineConfig";

import { IconBrandInstagram, IconMail } from "@tabler/icons-react";

export default async function Footer() {
  const config = await getCanineConfig();

  return (
    <div className="w-full bg-gray-100 dark:bg-zinc-800 pt-8 pb-4">
      <div className="w-full max-w-[1300px] mx-auto px-4">
        <div className="flex justify-between items-center">
          <img src="/logo.svg" className="h-8 block dark:hidden" />
          <img src="/logo-inv.svg" className="h-8 hidden dark:block" />
          <p className="font-semibold text-sm text-gray-500 dark:text-gray-400">
            VSETH
          </p>
        </div>
        <div className="flex gap-4 my-2">
          <a
            target="_blank"
            href={config.socialMedia[0].link}
            style={{ backgroundColor: config.primaryColor }}
            className="rounded-full p-2 text-white"
          >
            <IconBrandInstagram />
          </a>
          <a
            target="_blank"
            href={config.socialMedia[0].link}
            style={{ backgroundColor: config.primaryColor }}
            className="rounded-full p-2 text-white"
          >
            <IconMail />
          </a>
        </div>
        <div className="border-t-[6px] border-t-black dark:border-t-gray-500 w-full my-4" />
        <div className="flex justify-between items-center text-gray-500 dark:text-gray-400">
          <span>{config.copyright}</span>
          <div className="flex gap-4">
            <a target="_blank" href={config.privacy} className="underline">
              Privacy Policy
            </a>
            <a target="_blank" href={config.disclaimer} className="underline">
              Impressum
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
