import { Box, Button } from "@mantine/core";

import DateFormatter from "../components/dateFormatter";

import { Icon, ICONS } from "vseth-canine-ui";

const ViewEntry = (props: any) => {
  const entry = props.entry;
  const lang = props.lang;

  const isFeatured = entry.isFeatured;
  console.log(props);
  const col = isFeatured ? "#f03a47" : "#009fe3";
  const colBright = isFeatured ? "#fee2e2" : "#e0f2fe";

  return (
    <Box>
      <div
        style={{
          borderTop: "solid 2px " + col,
          marginTop: "30px",
          backgroundColor: colBright,
          padding: "8px",
        }}
      >
        <p style={{ fontSize: "16pt" }}>
          {entry.organization != "" && <span>{entry.organization}</span>}
          {(lang == "de" ? entry.titleDE : entry.titleEN) != "" && (
            <span>
              :
              <b style={{ color: col }}>
                {" "}
                {lang == "de" ? entry.titleDE : entry.titleEN}{" "}
                {isFeatured && (
                  <Icon icon={ICONS.STAR_FILLED} color={col} size={16} />
                )}
              </b>
            </span>
          )}
        </p>
        <p>
          {
            <DateFormatter
              startAt={entry.startAt}
              endAt={entry.endAt}
              lang={lang}
              place={entry.place}
            />
          }
        </p>
      </div>
      <div style={{ paddingLeft: "8px", paddingRight: "8px" }}>
        <p style={{ marginTop: "10px" }}>
          {lang == "de" ? entry.descriptionDE : entry.descriptionEN}
        </p>
        {entry.contact != "" && (
          <p style={{ marginTop: "8px" }}>
            {lang == "de" ? "Kontakt" : "Contact Us"}:{" "}
            <a
              href={"mailto:" + entry.contact}
              style={{ textDecoration: "underline" }}
            >
              {entry.contact}
            </a>
          </p>
        )}
        <p style={{ marginTop: "8px" }}>
          {entry.signUp != "" && (
            <a
              href={entry.signUp}
              target="_blank"
              rel="noreferrer"
              style={{ marginRight: "4px" }}
            >
              <Button color={isFeatured && "red"}>
                {lang == "de" ? "Anmeldung" : "Sign Up"} ➜
              </Button>
            </a>
          )}
          {entry.website != "" && (
            <a href={entry.website} target="_blank" rel="noreferrer">
              <Button color={isFeatured && "red"}>
                {lang == "de" ? "Webseite" : "Website"} ➜
              </Button>
            </a>
          )}
        </p>
      </div>
    </Box>
  );
};

export default ViewEntry;
