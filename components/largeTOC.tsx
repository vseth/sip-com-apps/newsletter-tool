"use client";

import { Grid, useMantineTheme, useComputedColorScheme } from "@mantine/core";

import { getTextColor, getPrimaryColor } from "@/utilities/colors";

import useConfig from "@/config/useConfig";

import { EntryType } from "@/interfaces/newsletter";

export default function LargeTOC({
  entries,
  locale,
}: {
  entries: EntryType[];
  locale: "de" | "en";
}) {
  const config = useConfig();

  const theme = useMantineTheme();
  const cs = useComputedColorScheme();

  if (!config) return <></>;

  const entry_types = config?.newsletter?.entry_types || { en: [], de: [] };

  const entryTypes = entry_types["de"].map((et: string, i: number) => {
    return {
      value: et,
      label: config?.newsletter.entry_types[locale][i] || ("" as string),
    };
  });

  const tailwindClass = `md:grid-cols-${entryTypes.length}`;

  return (
    <div
      className={`grid grid-cols-1 sm:grid-cols-2 ${tailwindClass} items-start gap-4`}
    >
      {entryTypes.map((entryType: { value: string; label: string }) => (
        <div key={entryType.value}>
          <h3
            className="font-semibold text-lg"
            style={{ color: getPrimaryColor(theme, cs) }}
          >
            {entryType.label}
          </h3>
          {entries
            .filter((e) => !e.isDisabled)
            .filter((e) => e.entryType == entryType.value)
            .map((e) => (
              <p
                className="whitespace-nowrap truncate text-zinc-700 dark:text-zinc-300"
                key={e.id}
              >
                {e.organization}:{" "}
                {(locale == "de" ? e.titleDE : e.titleEN) != "" && (
                  <span style={{ color: getPrimaryColor(theme, cs) }}>
                    {locale == "de" ? e.titleDE : e.titleEN}
                  </span>
                )}
              </p>
            ))}
        </div>
      ))}
    </div>
  );
}
