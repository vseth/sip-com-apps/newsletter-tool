"use client";

import { useState } from "react";
import Link from "next/link";

import { useCurrentLocale, useScopedI18n } from "@/locales/client";

import dayjs from "dayjs";

import {
  ActionIcon,
  Alert,
  Box,
  Button,
  Card,
  Dialog,
  Group,
  Space,
  Spoiler,
  Text,
  Tooltip,
  useMantineTheme,
} from "@mantine/core";

import {
  IconTrash,
  IconX,
  IconEye,
  IconMail,
  IconPlus,
  IconClock,
  IconEdit,
  IconInfoCircle,
} from "@tabler/icons-react";

import LargeTOC from "@/components/largeTOC";

import useAuth from "@/utilities/useAuth";
import {
  datetimeFormatter,
  longDateFormatter,
} from "@/utilities/datetimeFormatter";
import { getTextColor } from "@/utilities/colors";

import useConfig from "@/config/useConfig";

import { gql, useMutation } from "@apollo/client";

const deleteNewsletterMut = gql`
  mutation deleteNewsletter($id: Int) {
    deleteNewsletter(id: $id) {
      id
    }
  }
`;

import type { NewsletterType } from "@/interfaces/newsletter";

export default function NewsletterCard({
  newsletter,
  largeTOC,
  session,
  refetch,
}: {
  newsletter: NewsletterType;
  largeTOC: boolean;
  session: any;
  refetch: () => void;
}) {
  const [dialogOpen, setDialogOpen] = useState(false);
  const locale = useCurrentLocale();
  const theme = useMantineTheme();

  const t = useScopedI18n("main");
  const config = useConfig();
  const access = useAuth(true);

  const [deleteNewsletter] = useMutation(deleteNewsletterMut);

  const correction_time = config?.newsletter?.correction_time || 48;

  const isDeletable = () => {
    if (newsletter.entries.filter((e) => !e.isDisabled).length == 0)
      return true;
    return false;
  };

  const deleteNL = async () => {
    await deleteNewsletter({
      variables: {
        id: newsletter.id,
      },
    });
    refetch();
    setDialogOpen(false);
  };

  return (
    <>
      <div className="p-4 rounded-md w-full bg-zinc-50 dark:bg-zinc-700 flex flex-col justify-between">
        <Box>
          <h3 className="font-semibold">
            {longDateFormatter(locale, newsletter.sendAt)}
          </h3>
          {!newsletter.isSent && (
            <p>
              {t("deadline")}:{" "}
              {datetimeFormatter(
                locale,
                dayjs(newsletter.sendAt)
                  .subtract(correction_time, "hour")
                  .subtract(1, "minute"),
              )}
            </p>
          )}
        </Box>
        <Space h="md" />

        {largeTOC ? (
          <LargeTOC entries={newsletter.entries} locale={locale} />
        ) : (
          <Box>
            <Spoiler showLabel={t("showMore")} hideLabel={t("hide")}>
              {newsletter.entries
                .filter((e) => !e.isDisabled)
                .map((e, i) => (
                  <p
                    key={i}
                    className="text-zinc-400 whitespace-nowrap truncate"
                  >
                    {e.organization}:{" "}
                    <span className="text-black">{e.titleDE}</span>
                  </p>
                ))}
            </Spoiler>
          </Box>
        )}
        <Space h="md" />

        {dayjs(new Date()) >
          dayjs(newsletter.sendAt).subtract(correction_time, "hour") &&
          !newsletter.isSent && (
            <>
              <Alert
                title={t("deadlineOver")}
                color="red"
                icon={<IconClock className="text-white" />}
                variant="filled"
                className="mb-5"
              >
                {t("deadlineOverDesc")}
              </Alert>
            </>
          )}

        {newsletter.isSent ? (
          <Group className="mt-4">
            <Link href={"/view/" + newsletter.id}>
              <Button leftSection={<IconEye className="text-white" />}>
                {t("view")}
              </Button>
            </Link>
          </Group>
        ) : (
          <Group gap={8} className="mt-4">
            <Link href={"/add/" + newsletter.id}>
              <Button leftSection={<IconPlus className="text-white" />}>
                {t("addEntry")}
              </Button>
            </Link>
            {access && (
              <>
                <Link href={"/view/" + newsletter.id}>
                  <Button leftSection={<IconEye />} variant="light">
                    {t("view")}
                  </Button>
                </Link>
                <Link href={"/edit/" + newsletter.id}>
                  <Button leftSection={<IconEdit />} variant="light">
                    {t("edit")}
                  </Button>
                </Link>
                <Link href={"/send/" + newsletter.id}>
                  <Button leftSection={<IconMail />} variant="light">
                    {t("send")}
                  </Button>
                </Link>
                <Button
                  leftSection={<IconTrash className="text-white" />}
                  color="red"
                  disabled={!isDeletable()}
                  onClick={() => setDialogOpen(true)}
                >
                  {t("delete")}
                </Button>
                {!isDeletable() && (
                  <Tooltip label={t("removeEntries")}>
                    <ActionIcon variant="transparent">
                      <IconInfoCircle className="text-black dark:text-zinc-300" />
                    </ActionIcon>
                  </Tooltip>
                )}
              </>
            )}
          </Group>
        )}
      </div>
      <Dialog
        opened={dialogOpen}
        withCloseButton
        onClose={() => setDialogOpen(false)}
        size="lg"
        radius="md"
      >
        <Text size="sm" style={{ marginBottom: 10 }}>
          {t("uSure")}
        </Text>

        <div className="flex gap-2 justify-end">
          <Button
            leftSection={<IconX />}
            onClick={() => setDialogOpen(false)}
            variant="light"
          >
            {t("cancel")}
          </Button>
          <Button
            leftSection={<IconTrash className="text-white" />}
            color="red"
            onClick={() => deleteNL()}
          >
            {t("delete")}
          </Button>
        </div>
      </Dialog>
    </>
  );
}
