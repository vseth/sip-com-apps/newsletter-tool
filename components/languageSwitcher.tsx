"use client";

import Link from "next/link";

import { Popover, PopoverButton, PopoverPanel } from "@headlessui/react";

import { IconChevronDown, IconChevronUp } from "@tabler/icons-react";

import { useChangeLocale, useCurrentLocale } from "@/locales/client";

export default function LanguageSwitcher({
  left = false,
  light = true,
}: {
  left?: boolean;
  light?: boolean;
}) {
  const locales = ["de", "en"] as const;
  const locale = useCurrentLocale();

  const changeLocale = useChangeLocale();

  return (
    <Popover className="relative group">
      <PopoverButton>
        <span
          className={`flex gap-4 ${light ? "text-zinc-200" : "text-zinc-700 dark:text-zinc-300"}`}
        >
          {locale.toUpperCase()}
          <IconChevronDown className="group-data-[open]:rotate-180" />
        </span>
      </PopoverButton>
      <PopoverPanel
        anchor={`bottom ${left ? "start" : "end"}`}
        className="flex flex-col bg-white rounded-md p-1 shadow-md min-w-24 dark:bg-zinc-800 z-50"
      >
        {locales.map((l) => (
          <button
            className="p-3 hover:bg-zinc-100 dark:hover:bg-zinc-700 text-zinc-700 dark:text-zinc-300 text-left rounded-md"
            onClick={() => changeLocale(l)}
            key={l}
          >
            {l.toUpperCase()}
          </button>
        ))}
      </PopoverPanel>
    </Popover>
  );
}
