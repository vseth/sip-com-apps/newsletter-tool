"use client";

import { useState } from "react";

import { Modal, Center, Space, Button } from "@mantine/core";
import { DatePicker } from "@mantine/dates";
import { notifications } from "@mantine/notifications";

import { IconCheck, IconPlus, IconX } from "@tabler/icons-react";

import { useScopedI18n } from "@/locales/client";

import { gql, useMutation } from "@apollo/client";

const addNewsletterMut = gql`
  mutation addNewsletter($sendAt: Date!) {
    addNewsletter(sendAt: $sendAt) {
      id
    }
  }
`;

export default function AddNewsletterModal({
  open,
  setOpen,
  refetch,
}: {
  open: boolean;
  setOpen: (open: boolean) => void;
  refetch: () => void;
}) {
  const [releaseDate, setReleaseDate] = useState<Date | null>(null);

  const [addNewsletter] = useMutation(addNewsletterMut);

  const t = useScopedI18n("main");

  const add = async (releaseDate: Date | null) => {
    if (releaseDate == null) {
      notifications.show({
        title: "Unable to add newsletter",
        message: "Please enter a release date",
        color: "red",
        icon: <IconX />,
      });
      return;
    }
    if (releaseDate < new Date()) {
      notifications.show({
        title: "Unable to add newsletter",
        message: "This date is in the past",
        color: "red",
        icon: <IconX />,
      });
      return;
    }

    await addNewsletter({
      variables: {
        sendAt: releaseDate,
      },
    });
    await refetch();
    notifications.show({
      title: "Success",
      message: t("newsletterWasAdded"),
      color: "green",
      icon: <IconCheck />,
    });
  };

  return (
    <Modal
      opened={open}
      withCloseButton
      onClose={() => setOpen(false)}
      size="xs"
      radius="md"
      title={t("addNewsletter")}
      centered
    >
      <Center>
        <DatePicker value={releaseDate} onChange={setReleaseDate} />
      </Center>
      <Space h="md" />
      <Button leftSection={<IconPlus />} onClick={() => add(releaseDate)}>
        {t("add")}
      </Button>
    </Modal>
  );
}
