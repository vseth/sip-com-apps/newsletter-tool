import dayjs from "dayjs";

const DateFormatter = (props) => {
  const startAt = props.startAt;
  const endAt = props.endAt;
  const place = props.place;

  return (
    <>
      {startAt && <span>{dayjs(startAt).format("ddd, DD.MM.YY, HH:mm")}</span>}
      {endAt &&
        dayjs(startAt).format("DDMMYY") == dayjs(endAt).format("DDMMYY") && (
          <span> – {dayjs(endAt).format("HH:mm")}</span>
        )}
      {endAt &&
        dayjs(startAt).format("DDMMYY") != dayjs(endAt).format("DDMMYY") && (
          <span> – {dayjs(endAt).format("ddd, DD.MM.YY, HH:mm")}</span>
        )}
      {place != "" && <span> @ {place}</span>}
    </>
  );
};

export default DateFormatter;
