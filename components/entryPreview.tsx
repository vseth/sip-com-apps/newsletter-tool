"use client";

import { Button, useMantineTheme, useComputedColorScheme } from "@mantine/core";

import {
  eventTimeFormatter,
  mergeTimeAndDate,
} from "@/utilities/datetimeFormatter";
import { getBackgroundColor, getPrimaryColor } from "@/utilities/colors";

import type { EntryType } from "@/interfaces/newsletter";

export interface EntryPreviewProps {
  data: EntryType;
  locale: string;
}

export default function EntryPreview({ data, locale }: EntryPreviewProps) {
  const theme = useMantineTheme();
  const cs = useComputedColorScheme("light");

  return (
    <div className="flex justify-between flex-col">
      <div
        style={{
          borderTop: "solid 2px " + getPrimaryColor(theme, cs),
          backgroundColor: getBackgroundColor(theme) + "44",
        }}
        className="p-2 rounded-sm mt-4"
      >
        <p style={{ fontSize: "16pt" }}>
          {data.organization != "" && <span>{data.organization}</span>}
          {(locale == "de" ? data.titleDE : data.titleEN) != "" && (
            <span>
              :
              <b style={{ color: getPrimaryColor(theme, cs) }}>
                {" "}
                {locale == "de" ? data.titleDE : data.titleEN}
              </b>
            </span>
          )}
        </p>

        <p>
          {eventTimeFormatter(
            locale || "en",
            mergeTimeAndDate(data.startAt, data.startTimeAt),
            data.startTimeAt ? true : false,
            mergeTimeAndDate(data.endAt, data.endTimeAt),
            data.endTimeAt ? true : false,
            data.place,
          )}
        </p>
      </div>
      <div
        style={{
          paddingLeft: "8px",
          paddingRight: "8px",
          marginBottom: "15px",
          flexGrow: 1,
        }}
      >
        <p style={{ marginTop: "10px" }}>
          {locale == "de" ? data.descriptionDE : data.descriptionEN}
        </p>
      </div>
      <div
        style={{
          paddingLeft: "8px",
          paddingRight: "8px",
          marginBottom: "15px",
        }}
      >
        {data.contact != "" && (
          <p style={{ marginTop: "8px" }}>
            {locale == "de" ? "Kontakt" : "Contact Us"}:{" "}
            <a
              href={"mailto:" + data.contact}
              style={{
                textDecoration: "underline",
                color: getPrimaryColor(theme, cs),
              }}
            >
              {data.contact}
            </a>
          </p>
        )}
        <p style={{ marginTop: "8px" }}>
          {data.signUp != "" && (
            <a
              href={data.signUp}
              target="_blank"
              rel="noreferrer"
              style={{ marginRight: "4px" }}
            >
              <Button>{locale == "de" ? "Anmeldung" : "Sign Up"} ➜</Button>
            </a>
          )}
          {data.website != "" && (
            <a href={data.website} target="_blank" rel="noreferrer">
              <Button>{locale == "de" ? "Webseite" : "Website"} ➜</Button>
            </a>
          )}
        </p>
      </div>
    </div>
  );
}
