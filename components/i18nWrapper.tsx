"use client";

import type { ReactNode } from "react";

import { ApolloProvider } from "@apollo/client";
import apolloClient from "@/lib/apollo";

import { I18nProviderClient } from "@/locales/client";

import { SessionProvider } from "next-auth/react";

import { MantineProvider } from "@mantine/core";
import { Notifications } from "@mantine/notifications";
import { useColorScheme, useMediaQuery } from "@mantine/hooks";

export default function I18nWrapper({
  locale,
  theme,
  children,
}: {
  locale: string;
  theme: any;
  children: ReactNode;
}) {
  const preferredColorScheme = useMediaQuery("(prefers-color-scheme: dark)");

  return (
    <I18nProviderClient locale={locale}>
      <ApolloProvider client={apolloClient}>
        <SessionProvider>
          <MantineProvider
            theme={theme}
            forceColorScheme={preferredColorScheme ? "dark" : "light"}
          >
            <Notifications />
            {children}
          </MantineProvider>
        </SessionProvider>
      </ApolloProvider>
    </I18nProviderClient>
  );
}
