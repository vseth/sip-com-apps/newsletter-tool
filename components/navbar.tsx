import Link from "next/link";

import NavDesktop from "@/components/navDesktop";
import NavMobile from "@/components/navMobile";
import I18nWrapper from "@/components/i18nWrapper";
import LoginButton from "@/components/loginButton";

import { getScopedI18n } from "@/locales/server";

import getConfig from "@/config/getConfig";
import getCanineConfig from "@/config/getCanineConfig";

export default async function Navbar({ locale }: { locale: "de" | "en" }) {
  const t = await getScopedI18n("nav");

  const config = await getConfig();
  const canineConfig = await getCanineConfig();

  const navItems = [
    {
      label: t("newEntry"),
      href: "/add",
      match: "/(de|en)/add/?.*",
    },
    {
      label: t("viewNewest"),
      href: "/view",
      match: "/(de|en)/view/?.*",
    },
  ];

  return (
    <>
      <div
        style={{ backgroundColor: canineConfig.primaryColor }}
        className="hidden md:block w-full h-14 px-4"
      >
        <div className="flex justify-between items-center h-full">
          <img src={config.theme["logo-mono"]} className="h-8 invert" />

          <NavDesktop items={canineConfig.externalNav} locale={locale} />
        </div>
      </div>
      <div className="w-full bg-white dark:bg-zinc-900 shadow-md h-14 px-4 sticky top-0 mb-16 bg-opacity-30 dark:bg-opacity-30 backdrop-blur-lg backdrop-filter hidden md:block z-40 dark:border-b border-b-zinc-400">
        <div className="flex justify-between items-center h-full">
          <Link className="text-xl text-zinc-700 dark:text-zinc-300" href="/">
            {config.general.name}
          </Link>
          <div className="md:flex hidden gap-8 text-zinc-700 dark:text-zinc-300 items-center">
            {navItems.map((item, i) => (
              <Link href={item.href} key={i}>
                {item.label}
              </Link>
            ))}

            <I18nWrapper locale={locale} theme={undefined}>
              <LoginButton primary={config.theme.main_colors[6]} />
            </I18nWrapper>
          </div>
        </div>
      </div>
      <I18nWrapper locale={locale} theme={undefined}>
        <NavMobile
          items={navItems}
          locale={locale}
          signet="/signet-mono-inv.svg"
          primary={canineConfig.primaryColor}
          title={config.general.name}
        />
      </I18nWrapper>
    </>
  );
}
