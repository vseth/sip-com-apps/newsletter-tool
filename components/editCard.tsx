import { useRouter } from "next/navigation";

import { useScopedI18n, useCurrentLocale } from "@/locales/client";

import {
  Alert,
  Box,
  Button,
  Card,
  Grid,
  Group,
  Space,
  Tooltip,
  useMantineTheme,
} from "@mantine/core";

import {
  IconClock,
  IconCheck,
  IconMail,
  IconStar,
  IconX,
  IconEdit,
} from "@tabler/icons-react";

import { eventTimeFormatter } from "@/utilities/datetimeFormatter";

import type { EntryType } from "@/interfaces/newsletter";

type EditFunction = (_: number) => void;
interface IOrgType {
  value: string;
  label: string;
}
interface IEditCard {
  entry: EntryType;
  orgTypes: IOrgType[];
  isTooLate: boolean;
  edit: EditFunction;
}

const EditCard = ({ entry, orgTypes, isTooLate, edit }: IEditCard) => {
  const locale = useCurrentLocale();
  const theme = useMantineTheme();
  const t = useScopedI18n("edit");

  return (
    <Card
      shadow="sm"
      p="lg"
      radius="md"
      withBorder
      key={entry.id}
      className="mb-5"
    >
      <div className="grid grid-cols-1 md:grid-cols-3 gap-4">
        <div className="flex flex-col justify-between">
          <Box>
            <p style={{ fontSize: "16pt" }}>
              <span>{entry.organization}</span>
              <span>
                :{" "}
                <b
                  style={{
                    color:
                      theme.colors[theme.primaryColor][
                        theme.primaryShade as number
                      ],
                  }}
                >
                  {entry.titleDE} / {entry.titleEN}
                </b>
              </span>
            </p>

            <p>
              {eventTimeFormatter(
                locale || "en",
                entry.startAt,
                entry.includeStartTime,
                entry.endAt,
                entry.includeEndTime,
                entry.place,
              )}
            </p>

            <p>
              {
                orgTypes.filter((ot) => entry.organizationType == ot.value)[0]
                  ?.label
              }
            </p>

            <Space h="md" />

            {entry.signUp && (
              <p>
                {t("SignUp")}:{" "}
                <a
                  href={entry.signUp}
                  style={{
                    textDecoration: "underline",
                    color:
                      theme.colors[theme.primaryColor][
                        theme.primaryShade as number
                      ],
                  }}
                >
                  {entry.signUp}
                </a>
              </p>
            )}
            {entry.website && (
              <p>
                {t("Website")}:{" "}
                <a
                  href={entry.website}
                  style={{
                    textDecoration: "underline",
                    color:
                      theme.colors[theme.primaryColor][
                        theme.primaryShade as number
                      ],
                  }}
                >
                  {entry.website}
                </a>
              </p>
            )}
            {entry.contact && (
              <p>
                {t("ContactUs")}:{" "}
                <a
                  href={"mailto:" + entry.contact}
                  style={{
                    textDecoration: "underline",
                    color:
                      theme.colors[theme.primaryColor][
                        theme.primaryShade as number
                      ],
                  }}
                >
                  {entry.contact}
                </a>
              </p>
            )}

            <Space h="md" />
          </Box>

          <Group>
            <Button
              leftSection={<IconEdit />}
              onClick={() => edit(entry.id!)}
              className="my-3"
            >
              {t("Edit")}
            </Button>
            <a href={"mailto:" + entry.authorMail}>
              <Tooltip label={t("Contact") + " " + entry.author}>
                <Button variant="light" leftSection={<IconMail />}>
                  {t("E-Mail")}
                </Button>
              </Tooltip>
            </a>
          </Group>
        </div>
        <div>
          <p style={{ marginBottom: "10px" }}>{entry.descriptionDE}</p>
          <p>{entry.descriptionEN}</p>
        </div>
        <div>
          {entry.isFeatured && (
            <Alert
              color="yellow"
              title={t("Featured")}
              icon={<IconStar />}
              variant="filled"
              className="mb-3"
            >
              {t("FeaturedDesc")}
            </Alert>
          )}
          {entry.isChecked && (
            <Alert
              color="green"
              title={t("Checked")}
              icon={<IconCheck />}
              variant="filled"
              className="mb-3"
            >
              {t("CheckedDesc")}
            </Alert>
          )}
          {entry.isDisabled && (
            <Alert
              color="gray"
              title={t("Disabled")}
              icon={<IconX />}
              variant="filled"
              className="mb-3"
            >
              {t("DisabledDesc")}
            </Alert>
          )}
          {isTooLate && (
            <Alert
              title={t("TooLate")}
              color="red"
              icon={<IconClock />}
              variant="filled"
              className="mb-3"
            >
              {t("TooLateDesc")}
            </Alert>
          )}
        </div>
      </div>
    </Card>
  );
};

export default EditCard;
