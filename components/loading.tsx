import { IconLoader2 } from "@tabler/icons-react";

export default function Loader() {
  return <IconLoader2 size={30} className="animate-spin" />;
}
