import { useState, useEffect } from "react";

import { useScopedI18n, useCurrentLocale } from "@/locales/client";

import {
  Modal,
  Button,
  Select,
  Switch,
  Textarea,
  TextInput,
  useMantineTheme,
} from "@mantine/core";

import {
  IconLocation,
  IconCalendar,
  IconClock,
  IconCheck,
  IconX,
  IconStar,
  IconDeviceFloppy,
  IconLink,
  IconMail,
} from "@tabler/icons-react";

import { DatePickerInput, TimeInput } from "@mantine/dates";

import { useForm } from "@mantine/form";

import Loading from "@/components/loading";
import {
  dateFormatter,
  stringToDatetime,
  mergeTimeAndDate,
  stringToTimeString,
} from "@/utilities/datetimeFormatter";
import { getTextColor } from "@/utilities/colors";

import useConfig from "@/config/useConfig";

import { gql, useMutation, useQuery } from "@apollo/client";

const editEntryMut = gql`
  mutation editEntry(
    $id: Int!
    $newsletterId: Int!
    $organization: String!
    $organizationType: String!
    $titleDE: String!
    $titleEN: String!
    $descriptionDE: String!
    $descriptionEN: String!
    $entryType: String!
    $place: String
    $startAt: Date
    $includeStartTime: Boolean!
    $endAt: Date
    $includeEndTime: Boolean!
    $signUp: String
    $website: String
    $contact: String
    $isFeatured: Boolean!
    $isChecked: Boolean!
    $isDisabled: Boolean!
  ) {
    editEntry(
      id: $id
      newsletterId: $newsletterId
      organization: $organization
      organizationType: $organizationType
      titleDE: $titleDE
      titleEN: $titleEN
      descriptionDE: $descriptionDE
      descriptionEN: $descriptionEN
      entryType: $entryType
      place: $place
      startAt: $startAt
      includeStartTime: $includeStartTime
      endAt: $endAt
      includeEndTime: $includeEndTime
      signUp: $signUp
      website: $website
      contact: $contact
      isFeatured: $isFeatured
      isChecked: $isChecked
      isDisabled: $isDisabled
    ) {
      id
    }
  }
`;

const getNewslettersQry = gql`
  query getNewsletters {
    getNewsletters {
      id
      sendAt
    }
  }
`;

import type { NewsletterType, EntryType } from "@/interfaces/newsletter";

const EditModal = ({
  open,
  data,
  newsletterId,
  close,
  refetch,
}: {
  open: boolean;
  data: EntryType;
  newsletterId: string;
  close: () => void;
  refetch: () => void;
}) => {
  const theme = useMantineTheme();

  const locale = useCurrentLocale();
  const t = useScopedI18n("edit");
  const tf = useScopedI18n("form");
  const config = useConfig();

  const [newsletters, setNewsletters] = useState<
    { value: string; label: string }[]
  >([]);

  const [editEntry] = useMutation(editEntryMut);
  useQuery(getNewslettersQry, {
    onCompleted: (data) => {
      formatNewsletters(data);
    },
  });

  const form = useForm({
    initialValues: {
      newsletterId: "",
      organization: "",
      organizationType: "",
      titleDE: "",
      titleEN: "",
      descriptionDE: "",
      descriptionEN: "",
      entryType: "",
      place: "",
      startAt: new Date(),
      startTimeAt: "",
      endAt: new Date(),
      endTimeAt: "",
      signUp: "",
      website: "",
      contact: "",
      isFeatured: false,
      isChecked: true,
      isDisabled: false,
    },

    transformValues: (values) => ({
      ...values,
      startAt: values.startAt
        ? mergeTimeAndDate(values.startAt, values.startTimeAt || "")
        : null,
      endAt: values.endAt
        ? mergeTimeAndDate(values.endAt, values.endTimeAt || "")
        : null,
    }),

    /*
     * Possible combinations of startAt, startTimeAt, endAt, endTimeAt:
     *
     * - All four are set
     * - Only startAt and startTimeAt are set
     * - Only startAt is set
     * - Only startAt and endAt are set
     * - None are set
     *
     * FIXME: Implement this into validate
     */
    validate: {
      newsletterId: (value) => (value ? null : tf("Enewsletter")),
      organization: (value) => (value.length < 1 ? tf("Eorg") : null),
      organizationType: (value) => (value == "" ? tf("EorgType") : null),
      titleDE: (value) => (value.length < 1 ? tf("Etitle") : null),
      titleEN: (value) => (value.length < 1 ? tf("Etitle") : null),
      descriptionDE: (value) =>
        value.length < 1
          ? tf("Edesc")
          : value.length > 400
            ? tf("EtooLong")
            : null,
      descriptionEN: (value) =>
        value.length < 1
          ? tf("Edesc")
          : value.length > 400
            ? tf("EtooLong")
            : null,
      entryType: (value, values) =>
        !value
          ? tf("EentryType")
          : values.organizationType == "VSETH" && value != "VSETH"
            ? tf("EselVSETH")
            : values.organizationType != "VSETH" && value == "VSETH"
              ? tf("EnoVSETH")
              : null,
      startAt: (value, values) =>
        !value && (values.endAt || values.startTimeAt) ? tf("Edate") : null,
      startTimeAt: (value, values) =>
        !values.startAt && value ? tf("Etime") : null,
      endAt: (value, values) =>
        !value && values.endTimeAt ? tf("Edate") : null,
      endTimeAt: (value, values) =>
        values.endAt && values.startTimeAt && !value ? tf("Etime") : null,
      contact: (value) =>
        value == "" ||
        /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(
          value,
        )
          ? null
          : tf("Email"),
    },
  });

  /*
   * Save modified Entry to database and refetch all entries.
   */
  const save = async (props: any) => {
    await editEntry({
      variables: {
        ...props,
        newsletterId: Number(props.newsletterId),
        id: data.id,
        includeStartTime: props.startTimeAt ? true : false,
        includeEndTime: props.endTimeAt ? true : false,
      },
    });
    await refetch();
    close();
    return;
  };

  const cancel = () => {
    close();
  };

  /*
   * Anytime a new Entry should be edited, read its data and fill all the form fields.
   */
  const assign = () => {
    if (data) {
      form.setValues({
        ...data,
        newsletterId: String(newsletterId),
        startAt: data.startAt ? stringToDatetime(data.startAt) : undefined,
        startTimeAt:
          data.startAt && data.includeStartTime
            ? stringToTimeString(data.startAt)
            : undefined,
        endAt: data.endAt ? stringToDatetime(data.endAt) : undefined,
        endTimeAt:
          data.endAt && data.includeEndTime
            ? stringToTimeString(data.endAt)
            : undefined,
        isChecked: true,
      });
    }
  };

  useEffect(() => {
    assign();
  }, [data]);

  if (!config) return <Loading />;

  /*
   * Format newsletters in a value, label fashion for the Select component.
   */
  const formatNewsletters = async (newsletters: {
    getNewsletters: NewsletterType[];
  }) => {
    const d = newsletters.getNewsletters
      .filter((n) => !n.isSent)
      .map((n) => {
        const r = new Date(n.sendAt);
        const label = dateFormatter(locale, r);
        return { value: String(n.id), label: label };
      });
    setNewsletters(d);
  };

  const organization_types = config?.newsletter?.organization_types || {
    en: [],
    de: [],
  };
  const orgTypes = organization_types["en"].map((ot, i) => {
    return {
      value: config?.newsletter.organization_types.de[i] || "",
      label: config?.newsletter.organization_types[locale][i] || "",
    };
  });

  const entry_types = config?.newsletter?.entry_types || { en: [], de: [] };
  const entryTypes = entry_types["en"].map((et, i) => {
    return {
      value: config?.newsletter.entry_types.de[i],
      label: config?.newsletter.entry_types[locale][i],
    };
  });

  {
    /* FIXME: Make placeholders configurable and accessible to Add */
  }

  return (
    <Modal
      opened={open}
      title={<h2>{t("editEntry")}</h2>}
      size="xl"
      withCloseButton={false}
      onClose={() => close()}
    >
      <form onSubmit={form.onSubmit(save)}>
        <div className="grid grid-cols-1 sm:grid-cols-2 gap-4">
          <div className="col-span-1 sm:col-span-2">
            <Select
              label={tf("newsletter")}
              placeholder="01.01.2100"
              data={newsletters}
              {...form.getInputProps("newsletterId")}
              withAsterisk
            />
          </div>
          <div>
            <TextInput
              label={tf("org")}
              placeholder="Studi Lerngruppe XY"
              {...form.getInputProps("organization")}
              withAsterisk
            />
          </div>
          <div>
            <Select
              label={tf("orgType")}
              placeholder="Kommission"
              data={orgTypes}
              {...form.getInputProps("organizationType")}
            />
          </div>
          <div>
            <TextInput
              label={tf("titleDE")}
              placeholder="Vorstände Gesucht!"
              {...form.getInputProps("titleDE")}
              withAsterisk
            />
          </div>
          <div>
            <TextInput
              label={tf("titleEN")}
              placeholder="Board Members Wanted!"
              {...form.getInputProps("titleEN")}
              withAsterisk
            />
          </div>
          <div>
            <Textarea
              label={tf("descDE")}
              placeholder="Unsere Kommission sucht aktuell..."
              {...form.getInputProps("descriptionDE")}
              maxRows={5}
              minRows={5}
              autosize
              withAsterisk
            />
          </div>
          <div>
            <Textarea
              label={tf("descEN")}
              placeholder="Our committee is currently looking for..."
              {...form.getInputProps("descriptionEN")}
              maxRows={5}
              minRows={5}
              autosize
              withAsterisk
            />
          </div>
          <div>
            <Select
              label={tf("entryType")}
              placeholder="Other"
              data={entryTypes}
              {...form.getInputProps("entryType")}
              withAsterisk
            />
          </div>
          <div>
            <TextInput
              label={tf("place")}
              placeholder="CAB E 15.2"
              leftSection={<IconLocation />}
              {...form.getInputProps("place")}
            />
          </div>

          <div>
            <div className="flex gap-2">
              <DatePickerInput
                label={tf("startDate")}
                placeholder="01.01.2100"
                leftSection={<IconCalendar />}
                className="basis-4/6"
                {...form.getInputProps("startAt")}
              />
              <TimeInput
                label={tf("startTime")}
                leftSection={<IconClock />}
                className="basis-2/6"
                {...form.getInputProps("startTimeAt")}
              />
            </div>
          </div>
          <div>
            <div className="flex gap-2">
              <DatePickerInput
                label={tf("endDate")}
                placeholder="01.01.2100"
                leftSection={<IconCalendar />}
                className="basis-4/6"
                {...form.getInputProps("endAt")}
              />
              <TimeInput
                label={tf("endTime")}
                leftSection={<IconClock />}
                className="basis-2/6"
                {...form.getInputProps("endTimeAt")}
              />
            </div>
          </div>

          <div>
            <TextInput
              label={tf("signUp")}
              placeholder="https://bit.ly/ABCXYZ"
              leftSection={<IconLink />}
              {...form.getInputProps("signUp")}
            />
          </div>
          <div>
            <TextInput
              label={tf("website")}
              placeholder="https://blubb.vseth.ch"
              leftSection={<IconLink />}
              {...form.getInputProps("website")}
            />
          </div>
          <div>
            <TextInput
              label={tf("contactUs")}
              placeholder="contact@blubb.ethz.ch"
              leftSection={<IconMail />}
              {...form.getInputProps("contact")}
            />
          </div>

          <div className="col-span-1 sm:col-span-2 my-4">
            <div className="flex gap-4">
              <Switch
                label={tf("featured")}
                onLabel={<IconStar />}
                size="md"
                color="yellow"
                checked={form.values.isFeatured}
                onChange={(e) =>
                  form.setValues({ isFeatured: e.currentTarget.checked })
                }
              />
              <Switch
                label={tf("checked")}
                onLabel={<IconCheck />}
                size="md"
                color="green"
                checked={form.values.isChecked}
                onChange={(e) =>
                  form.setValues({ isChecked: e.currentTarget.checked })
                }
              />
              <Switch
                label={tf("disabled")}
                onLabel={<IconX />}
                size="md"
                color="red"
                checked={form.values.isDisabled}
                onChange={(e) =>
                  form.setValues({ isDisabled: e.currentTarget.checked })
                }
              />
            </div>
          </div>

          <div className="col-span-1 sm:col-span-2">
            <div className="flex gap-2 justify-end">
              <Button variant="light" leftSection={<IconX />} onClick={cancel}>
                {tf("cancel")}
              </Button>
              <Button type="submit" leftSection={<IconDeviceFloppy />}>
                {tf("save")}
              </Button>
            </div>
          </div>
        </div>
      </form>
    </Modal>
  );
};

export default EditModal;
