"use client";

import Link from "next/link";

import { Popover, PopoverButton, PopoverPanel } from "@headlessui/react";

import { IconMenu2, IconX } from "@tabler/icons-react";

import LanguageSwitcher from "@/components/languageSwitcher";
import LoginButton from "@/components/loginButton";

interface ItemType {
  label: string;
  href: string;
  match: string;
}

export default function NavMobile({
  signet,
  items,
  locale,
  primary,
  title,
}: {
  signet: string;
  items: ItemType[];
  locale: string;
  primary: string;
  title: string;
}) {
  return (
    <div
      className="w-full h-14 shadow-md px-4 flex items-center justify-between md:hidden text-white"
      style={{ backgroundColor: primary }}
    >
      <div className="flex gap-4 items-center">
        <img src={signet} className="h-8" />
        <Link href="/" className="text-white text-lg">
          {title}
        </Link>
      </div>

      <Popover className="relative h-full flex items-center group">
        <PopoverButton>
          <IconMenu2 className="group-data-[open]:hidden block" />
          <IconX className="group-data-[open]:block hidden" />
        </PopoverButton>
        <PopoverPanel
          anchor="bottom"
          className="flex flex-col bg-white dark:bg-zinc-800 shadow-md w-screen mt-4 p-1 overflow-visible"
        >
          {items.map((item, i) => (
            <Link
              key={i}
              href={item.href}
              className="p-3 text-zinc-700 dark:text-zinc-300 hover:bg-zinc-100 dark:hover:bg-zinc-700 rounded-md"
            >
              {item.label}
            </Link>
          ))}
          <div className="p-3 flex justify-between items-center">
            <LanguageSwitcher left={true} light={false} />
            <LoginButton primary={primary} />
          </div>
        </PopoverPanel>
      </Popover>
    </div>
  );
}
