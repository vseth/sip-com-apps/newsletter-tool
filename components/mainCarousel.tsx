"use client";

import { useRef } from "react";

import { useRouter } from "next/router";

import { useScopedI18n, useCurrentLocale } from "@/locales/client";

import {
  Box,
  Card,
  useMantineTheme,
  useComputedColorScheme,
} from "@mantine/core";

import { Carousel } from "@mantine/carousel";
import Autoplay from "embla-carousel-autoplay";

import EntryPreview from "@/components/entryPreview";

import { EntryType } from "@/interfaces/newsletter";

import { getStripeColor, getSurfaceColor } from "@/utilities/colors";

import { gql, useQuery } from "@apollo/client";

const getNewestNewsletterQry = gql`
  query {
    getNewestNewsletter {
      id
      sendAt
      entries {
        id
        titleDE
        titleEN
        descriptionDE
        descriptionEN
        organization
        startAt
        includeStartTime
        endAt
        includeEndTime
        place
        signUp
        website
        contact
        isFeatured
        isDisabled
      }
    }
  }
`;

export default function MainCarousel() {
  const { data } = useQuery(getNewestNewsletterQry);
  const autoplay = useRef(Autoplay({ delay: 6000 }));
  const locale = useCurrentLocale();
  const theme = useMantineTheme();
  const cs = useComputedColorScheme("light");
  const stripeColor = getStripeColor(theme);
  const stripeColor2 = cs === "light" ? "white" : "#27272a";
  const t = useScopedI18n("main");

  const pattern = `${stripeColor} 6%  14%,${stripeColor2} 16% 24%,${stripeColor} 26% 34%,${stripeColor2} 36% 44%, ${stripeColor} 46% 54%,${stripeColor2} 56% 64%,${stripeColor} 66% 74%,${stripeColor2} 76% 84%,${stripeColor} 86% 94%`;

  return (
    <div
      className="shadow-md w-full relative pt-full aspect-square rounded-md z-0"
      style={{
        background: `radial-gradient(100% 100% at 100% 0,${stripeColor2} 4%,${pattern},#0008 96%,#0000), radial-gradient(100% 100% at 0 100%,#0000, #0008 4%,${pattern},${stripeColor2} 96%)`,
        backgroundSize: "60px 60px",
      }}
    >
      <div className="absolute top-0 left-0 right-0 bottom-0 flex items-center">
        <div className="bg-white w-full dark:bg-zinc-800">
          <div>
            {data && data.getNewestNewsletter ? (
              <Carousel
                loop
                // @ts-ignore
                plugins={[autoplay.current]}
                onMouseEnter={autoplay.current.stop}
                onMouseLeave={autoplay.current.reset}
                slideGap="md"
                slideSize="70%"
              >
                {data.getNewestNewsletter.entries.map(
                  (entry: EntryType, i: number) => (
                    <Carousel.Slide key={i} style={{ display: "flex" }}>
                      <EntryPreview data={entry} locale={locale || "en"} />
                    </Carousel.Slide>
                  ),
                )}
              </Carousel>
            ) : (
              <div className="w-full h-48 flex items-center justify-center">
                <p>{t("noNewestNewsletter")}</p>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
