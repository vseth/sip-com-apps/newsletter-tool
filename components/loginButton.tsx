"use client";

import { useSession, signIn, signOut } from "next-auth/react";

import { IconLogin2, IconLogout } from "@tabler/icons-react";

import { Button, useMantineTheme } from "@mantine/core";

const LoginButton = ({ primary }: { primary: string }) => {
  const { data: session } = useSession();
  const theme = useMantineTheme();

  if (session) {
    return (
      <button
        onClick={() => signOut()}
        className="flex gap-2 items-center px-4 py-[6px] rounded-md text-white text-sm font-semibold"
        style={{ backgroundColor: primary }}
      >
        <IconLogout />
        Logout
      </button>
    );
  }
  return (
    <button
      onClick={() => signIn("keycloak")}
      className="flex gap-2 items-center px-4 py-[6px] rounded-md text-white text-sm font-semibold"
      style={{ backgroundColor: primary }}
    >
      <IconLogin2 />
      Login
    </button>
  );
};

export default LoginButton;
