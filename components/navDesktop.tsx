"use client";

import { Popover, PopoverButton, PopoverPanel } from "@headlessui/react";

import { IconChevronDown } from "@tabler/icons-react";

import LanguageSwitcher from "@/components/languageSwitcher";

interface LocalizedItemType {
  de: string;
  en: string;
}

interface ItemType {
  title: string | LocalizedItemType;
  childItems: LinkType[];
}

interface LinkType {
  title: string | LocalizedItemType;
  href: string;
}

export default function NavDesktop({
  items,
  locale,
}: {
  items: ItemType[];
  locale: "de" | "en";
}) {
  return (
    <div className="flex gap-8 uppercase text-zinc-200">
      {items.map((item, i) => (
        <Popover key={i} className="relative group">
          <PopoverButton>
            <span key={i} className="flex gap-4 uppercase">
              {typeof item.title === "object" ? item.title[locale] : item.title}
              <IconChevronDown className="group-data-[open]:rotate-180" />
            </span>
          </PopoverButton>
          <PopoverPanel
            anchor="bottom end"
            className="flex flex-col bg-white rounded-md p-1 shadow-md min-w-48 dark:bg-zinc-800 z-50"
          >
            {item.childItems.map((child, j) => (
              <a
                key={j}
                href={child.href}
                className="p-3 rounded-md hover:bg-zinc-100 dark:hover:bg-zinc-700 text-zinc-700 dark:text-zinc-300"
              >
                {typeof child.title === "object"
                  ? child.title[locale]
                  : child.title}
              </a>
            ))}
          </PopoverPanel>
        </Popover>
      ))}
      <LanguageSwitcher />
    </div>
  );
}
