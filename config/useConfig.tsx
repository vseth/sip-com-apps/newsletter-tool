import axios from "axios";

import { useState, useEffect } from "react";

import type { ConfigType } from "@/interfaces/config";

const useConfig = () => {
  const [config, setConfig] = useState<ConfigType | null>(null);

  const getConfig = async () => {
    const res = await axios.get("/api/config");

    setConfig(JSON.parse(res.data));
  };

  useEffect(() => {
    (async () => {
      await getConfig();
    })();
  }, []);

  return config;
};

export default useConfig;
