import getConfig from "./getConfig";

const getCanineConfig = async () => {
  const config = await getConfig();
  const res = await fetch(config.theme.config, {
    next: {
      revalidate: 86400,
    },
  });

  return res.json();
};

export default getCanineConfig;
