// import config from "./vseth-config.json";

import type { ConfigType } from "@/interfaces/config";

const getConfig = async (): Promise<ConfigType | never> => {
  const res = await fetch(
    process.env.NEWSLETTER_CONFIG ||
      "https://static.vseth.ethz.ch/assets/vseth-0000-vseth/newsletter.json",
    {
      next: {
        revalidate: 86400,
      },
    },
  );

  const config = await res.json();

  if (config.theme.main_colors.length !== 10)
    throw new Error("config.theme.main_colors needs exactly 10 colors");

  return config;
};

export default getConfig;
