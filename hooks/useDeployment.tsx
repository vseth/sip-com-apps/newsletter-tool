import axios from "axios";

import { useState, useEffect } from "react";

const useDeployment = () => {
  const [dep, setDep] = useState<string>("prod");

  const getDeployment = async () => {
    const res = await axios.get("/api/deployment");

    setDep(res.data.deployment);
  };

  useEffect(() => {
    (async () => {
      await getDeployment();
    })();
  }, []);

  return dep;
};

export default useDeployment;
